﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class EmpleadosController
    {
        //private static readonly string MESSAGE = "No hay datos para sincronizar";
        public static async Task<bool> SetEmpleados()
        {
            string setEmpleadosResponse = await EmpleadosServices.SetEmpleados();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setEmpleadosResponse, Formatting.Indented), false).Wait();
            return setEmpleadosResponse.Contains(JsonConst.MESSAGE_FAIL);
        }
    }
}
