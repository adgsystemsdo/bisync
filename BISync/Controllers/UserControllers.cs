﻿using System.Threading.Tasks;
using BISync.Services;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    public class UserControllers
    {
        public static async Task<JObject> Login()
        {
            return await UserServices.Login();
        }

        static public bool ResetUserToken()
        {
            return UserServices.resetUserToken();
        }

        static public async Task<JObject> LoggedUserInfo()
        {
            return await UserServices.LoggedUserInfo();
        }

        static public async Task<bool> LoginAsync(JObject Json)
        {
            return await UserServices.LoginAsync(Json);
        }
    }
}
