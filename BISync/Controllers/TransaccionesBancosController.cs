﻿using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class TransaccionesBancosController
    {
        public static async Task<bool> SetTransaccionesBancos()
        {
            string setTransaccionesBancosResponse = await TransaccionesBancosServices.SetTransaccionesBancos();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setTransaccionesBancosResponse, Formatting.Indented), false).Wait();
            return setTransaccionesBancosResponse.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}
