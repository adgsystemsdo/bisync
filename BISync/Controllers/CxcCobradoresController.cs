﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class CxcCobradoresController
    {
        public static async Task<bool> SetCxcCobrados()
        {
            string setCxcCobrados = await CxcCobradoresServices.SetCxcCobradores();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcCobrados, Formatting.Indented), false).Wait();
            return setCxcCobrados.Contains(JsonConst.MESSAGE_FAIL);
            
        }

    }
}
