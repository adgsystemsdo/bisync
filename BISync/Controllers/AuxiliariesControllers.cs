﻿using BISync.Helpers;
using BISync.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class AuxiliariesControllers
    {
        public static async Task<bool> SetAuxiliaries()
        {
            JObject setAuxiliariesResponse = await SynchronizingAuxiliaries.SetAuxiliaries();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setAuxiliariesResponse, Formatting.Indented), false).Wait();
            return ((string)setAuxiliariesResponse.SelectToken("status") == "success");

        }
    }
} 