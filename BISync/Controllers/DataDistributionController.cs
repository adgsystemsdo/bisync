﻿using BISync.Services;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    public class DataDistributionController
    {
        public static async Task<bool> DataSaleDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataSaleDistributionService().Rows[0][0]);
        }

       public static async Task<bool> DataCuadreCajaDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataCuadreDistributionServices().Rows[0][0]);
        }
     
        public static async Task<bool> DataCxcFacturasDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataCxcFacturasServices().Rows[0][0]);
        }

        public static async Task<bool> DataCxpFacturasDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataCxpFacturasServices().Rows[0][0]);
        }

        public static async Task<bool> DataInvComprasController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataInvCompraServices().Rows[0][0]);
        }

        public static async Task<bool> DataInvDevComprasDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataInvDevCompraServices().Rows[0][0]);
        }

        public static async Task<bool> DataCxcHeadMovDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataCxcMovHeadServices().Rows[0][0]);
        }

        public static async Task<bool> DataCxcItemMovDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataCxcMovItemServices().Rows[0][0]);
        }

        public static async Task<bool> DataCxpHeadMovDistributionController()
        {
            return await Task.FromResult((bool)DataDistributionService.DataCxpMovHeadServices().Rows[0][0]);
        }

        public static async Task<bool> DistribucionOnCloud()
        {
            return await DataDistributionService.ExecuteDistributionOnCloud() ;
        }

    }
}