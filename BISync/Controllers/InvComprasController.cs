﻿using System;
using System.Collections.Generic;
using System.Text;
using BISync.Services;
using BISync.Helpers;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class InvComprasController
    {
        //private static readonly string MESSAGE = "No hay datos para sincronizar";

        public static async Task<bool> SetInvComprasDiarias()
        {
            string setInvComprasDiariasResponse = await InvComprasServices.SetInvComprasDiarias();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvComprasDiariasResponse, Formatting.Indented), false).Wait();
            return setInvComprasDiariasResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetInvComprasMensuales()
        {
            string setInvComprasMensualesResponse = await InvComprasServices.SetInvComprasMensuales() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvComprasMensualesResponse, Formatting.Indented), false).Wait();
            return setInvComprasMensualesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetInvComprasAnuales()
        {
            string setInvComprasAnualResponse = await InvComprasServices.SetInvComprasAnuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvComprasAnualResponse, Formatting.Indented), false).Wait();
            return setInvComprasAnualResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetInvCompraHistorico()
        {
            string setInvCompraHistoricoResponse = await InvComprasServices.SetInvCompraHistorial();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvCompraHistoricoResponse, Formatting.Indented), false).Wait();
            return setInvCompraHistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

    }

}
