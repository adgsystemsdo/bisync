﻿using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class CuadreController
    {
        public static async Task<bool> SetCuadreCajaDiario()
        {
            string setCuadreCajaDiarioResponse = await CuadreCajaServices.SetCuadreCajaDiario();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCuadreCajaDiarioResponse, Formatting.Indented), false).Wait();
            return setCuadreCajaDiarioResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetCuadreCajaMensual()
        {
            string setCuadreCajaMensualResponse = await CuadreCajaServices.SetCuadreCajaMensual();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCuadreCajaMensualResponse, Formatting.Indented), false).Wait();
            return setCuadreCajaMensualResponse.Contains(JsonConst.MESSAGE_FAIL) ;
            
        }

        public static async Task<bool> SetCuadreCajaAnual()
        {
            string setCuadreCajaAnualResponse = await CuadreCajaServices.SetCuadreCajaAnual();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCuadreCajaAnualResponse, Formatting.Indented), false).Wait();
            return setCuadreCajaAnualResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCuadreCajaHistorico()
        {
            string setCuadreCajaMensualResponse = await CuadreCajaServices.SetCuadreCajahistorico();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCuadreCajaMensualResponse, Formatting.Indented), false).Wait();
            return setCuadreCajaMensualResponse.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}