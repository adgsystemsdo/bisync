﻿using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class CxcZonasController
    {
        //private static readonly string MESSAGE = "No hay datos para sincronizar";
        public static async Task<bool> SetCxcZonas()
        {
            string setCxcZonas = await CxcZonasServices.SetCxcZonas();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcZonas, Formatting.Indented), false).Wait();
            return setCxcZonas.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}