﻿using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class BancosControllers
    {
        public static async Task<bool> SetBancos()
        {
            string setBancosResponse = await BancosServices.SetBancos();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setBancosResponse, Formatting.Indented), false).Wait();
            return setBancosResponse.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}
