﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class InvDevComprasController
    {
        public static async Task<bool> SetInvDevComprasDiarias()
        {
            string setInvDevComprasDiariasResponse = await InvDevComprasServices.SetInvDevComprasDiarias();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvDevComprasDiariasResponse, Formatting.Indented), false).Wait();
            return setInvDevComprasDiariasResponse.Contains(JsonConst.MESSAGE_FAIL);

        }

        public static async Task<bool> SetInvDevComprasMensuales()
        {
            string setInvDevComprasMensualesResponse = await InvDevComprasServices.SetInvDevComprasMensuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvDevComprasMensualesResponse, Formatting.Indented), false).Wait();
            return setInvDevComprasMensualesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetInvDevComprasAnuales()
        {
            string setInvDevComprasAnualResponse = await InvDevComprasServices.SetInvDevComprasAnuales() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvDevComprasAnualResponse, Formatting.Indented), false).Wait();
            return setInvDevComprasAnualResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetInvDevCompraHistorico()
        {
            string setInvDevCompraHistoricoResponse = await InvDevComprasServices.SetInvDevCompraHistorial();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvDevCompraHistoricoResponse, Formatting.Indented), false).Wait();
            return setInvDevCompraHistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }


    }
}
