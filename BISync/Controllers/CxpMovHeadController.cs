﻿using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class CxpMovHeadController
    {
        public static async Task<bool> SetCxpMovHeadDiarias()
        {
            string setCxcMovHeadDiariasResponse = await CxpMovHeadServices.SetCxpMovHeadDiaria() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadDiariasResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadDiariasResponse.Contains(JsonConst.MESSAGE_FAIL) ;

        }

        public static async Task<bool> SetCxpMovHeadMensual()
        {
            string setCxcMovHeadMensualResponse = await CxpMovHeadServices.SetCxpMovHeadMensual() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadMensualResponse, Formatting.Indented), false).Wait();
            return  setCxcMovHeadMensualResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCxpMovHeadAnuales()
        {
            string setCxcMovHeadAnualesResponse = await CxpMovHeadServices.SetCxpMovHeadAnuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadAnualesResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadAnualesResponse.Contains(JsonConst.MESSAGE_FAIL);

        }

        public static async Task<bool> SetCxpMovHeadHistorico()
        {
            string setCxcMovHeadhistoricoResponse = await CxpMovHeadServices.SetCxpMovHeadHistorico();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadhistoricoResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadhistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);
        }
    }
}