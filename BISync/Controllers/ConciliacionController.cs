﻿using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class ConciliacionController
    {
        public static async Task<bool> SetConciliaciones()
        {
            string setConciliacionResponse = await ConciliacionServices.SetConciliaciones();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setConciliacionResponse, Formatting.Indented), false).Wait();
            return setConciliacionResponse.Contains(JsonConst.MESSAGE_FAIL);

        }

    }
}