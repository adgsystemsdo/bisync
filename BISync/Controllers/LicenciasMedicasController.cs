﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class LicenciasMedicasController
    {
        public static async Task<bool> SetNomLicenciaMedicas()
        {
            string setInvComprasDiariasResponse = await LicenciasMedicasServices.SetNomLicenciaMedicas(); 
            SettingsGetter.Notify(JsonConvert.SerializeObject(setInvComprasDiariasResponse, Formatting.Indented), false).Wait();
            return setInvComprasDiariasResponse.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}
