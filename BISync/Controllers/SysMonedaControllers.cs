﻿using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class SysMonedaControllers
    {
        public static async Task<bool> SetSysMoneda()
        {
            string setMonedaResponse = await SysMonedaServices.SetSysMonedas();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setMonedaResponse, Formatting.Indented), false).Wait();
            return setMonedaResponse.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}
