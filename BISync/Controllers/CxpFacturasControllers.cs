﻿using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class CxpFacturasControllers
    {
        //private static readonly string MESSAGE = "No hay datos para sincronizar";
        public static async Task<bool> SetCxpFacturaDiarias()
        {
            string setCxpFacturaDiariasResponse = await CxpFacturasServices.SetCxpFacturaDiaria();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxpFacturaDiariasResponse, Formatting.Indented), false).Wait();
            return setCxpFacturaDiariasResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCxpFacturaMensual()
        {
            string setCxpFacturaMensualResponse = await CxpFacturasServices.SetCxpFacturaMensual();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxpFacturaMensualResponse, Formatting.Indented), false).Wait();
            return setCxpFacturaMensualResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCxpFacturaAnuales()
        {
            string setCxpFacturaAnualesResponse = await CxpFacturasServices.SetCxpFacturasAnuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxpFacturaAnualesResponse, Formatting.Indented), false).Wait();
            return setCxpFacturaAnualesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetCxpFacturaHistorico()
        {
            string setCxpFacturashistoricoResponse = await CxpFacturasServices.SetCxpFacturashistorico();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxpFacturashistoricoResponse, Formatting.Indented), false).Wait();
            return setCxpFacturashistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

    }
}