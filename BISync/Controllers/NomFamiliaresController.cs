﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class NomFamiliaresController
    {
        public static async Task<bool> SetNomFamiliares()
        {
            string setNomFamiliaresResponse = await NomFamiliaresServices.SetNomFamiliares() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setNomFamiliaresResponse, Formatting.Indented), false).Wait();
            return setNomFamiliaresResponse.Contains(JsonConst.MESSAGE_FAIL);
        }
    }
}