﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class VacacionesController
    {
        private static readonly string MESSAGE = "No hay datos para sincronizar";
        public static async Task<bool> SetVacaciones()
        {
            string setVacacionesResponse = await VacacionesServices.SetVacaciones(); ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setVacacionesResponse, Formatting.Indented), false).Wait();
            return setVacacionesResponse.Contains(MESSAGE);
        }

    }
}
