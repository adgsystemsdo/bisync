﻿using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class InvGenDepCxcCuadresController
    {
        public static async Task<bool> SetDespositoSobreCuadres()
        {
            string setDepositoResponse = await InvGenDepCxcCuadresServices.SetDepositosSobreCuadres();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setDepositoResponse, Formatting.Indented), false).Wait();
            return setDepositoResponse.Contains(JsonConst.MESSAGE_FAIL);
        }
    }
}
