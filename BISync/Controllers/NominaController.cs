﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Services;
using BISync.Helpers;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class NominaController
    {
        public static async Task<bool> SetNomina()
        {
            string setEncabezadoNominaResponse = await NominaServices.SetEncabezadoNomina();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setEncabezadoNominaResponse, Formatting.Indented), false).Wait();

            string setDetalleNominaresponse = await NominaServices.SetDetalleNomina();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setDetalleNominaresponse, Formatting.Indented), false).Wait();

            return (setEncabezadoNominaResponse.Contains(JsonConst.MESSAGE_FAIL)) && (setDetalleNominaresponse.Contains(JsonConst.MESSAGE_FAIL));
        }
    }
}