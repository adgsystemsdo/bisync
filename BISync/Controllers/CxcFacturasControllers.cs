﻿using BISync.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Services;

namespace BISync.Controllers
{
    class CxcFacturasControllers
    {
        public static async Task<bool> SetCxcFacturaDiarias()
        {
            string setCxcFacturaDiariasResponse = await CxcFacturasServices.SetCxcFacturaDiaria();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcFacturaDiariasResponse, Formatting.Indented), false).Wait();
            return setCxcFacturaDiariasResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetCxcFacturaMensual()
        {
            string setCxcFacturaMensualResponse = await CxcFacturasServices.SetCxcFacturaMensual();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcFacturaMensualResponse, Formatting.Indented), false).Wait();
            return setCxcFacturaMensualResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetCxcFacturaAnuales()
        {
            string setCxcFacturaAnualesResponse = await CxcFacturasServices.SetCxcFacturasAnuales() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcFacturaAnualesResponse, Formatting.Indented), false).Wait();
            return setCxcFacturaAnualesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetCxcFacturaHistorico()
        {
            string setCxcFacturashistoricoResponse = await CxcFacturasServices.SetCxcFacturashistorico();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcFacturashistoricoResponse, Formatting.Indented), false).Wait();
            return setCxcFacturashistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);
        }
    }
}
