﻿using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;

namespace BISync.Controllers
{
    class CxcClientesControllers
    {
        public static async Task<bool> SetCxcClientes()
        {
            string setCxcClientesResponse = await ClientesServices.SetClientes(); 
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcClientesResponse, Formatting.Indented), false).Wait();
            return setCxcClientesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }
    }
}
