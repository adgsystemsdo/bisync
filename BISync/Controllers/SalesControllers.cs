﻿using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace BISync.Controllers
{
    class SalesControllers
    {
        public static async Task<bool> SetDailySales()
        {
            string setDailySalesResponse = await SalesServices.SetDailySales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setDailySalesResponse, Formatting.Indented), false).Wait();
            return setDailySalesResponse.Contains(JsonConst.MESSAGE_FAIL) ;
        }

        public static async Task<bool> SetMonthlySales()
        {
            string setMonthlySalesResponse = await SalesServices.SetMonthlySales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setMonthlySalesResponse, Formatting.Indented), false).Wait();
            return setMonthlySalesResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetYearlySales()
        {
            string setYearlySalesResponse = await SalesServices.SetYearlySales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setYearlySalesResponse, Formatting.Indented), false).Wait();
            return setYearlySalesResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SethistorySales()
        {
            string sethistorySalesResponse = await SalesServices.SethistorySales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(sethistorySalesResponse, Formatting.Indented), false).Wait();
            return sethistorySalesResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetDailyDSales()
        {
            string setDailyDSalesResponse = await SalesServices.SetDailyDSales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setDailyDSalesResponse, Formatting.Indented), false).Wait();
            return setDailyDSalesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetMonthlyDSales()
        {
            string setMonthlyDSalesResponse = await SalesServices.SetMonthlyDSales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setMonthlyDSalesResponse, Formatting.Indented), false).Wait();
            return setMonthlyDSalesResponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetYearlyDSales()
        {
            string setYearlyDSalesResponse = await SalesServices.SetYearlyDSales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setYearlyDSalesResponse, Formatting.Indented), false).Wait();
            return setYearlyDSalesResponse.Contains(JsonConst.MESSAGE_FAIL);

        }

        public static async Task<bool> SethistoryDSales()
        {
            string sethistoryDSalesResponse = await SalesServices.SethistoryDSales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(sethistoryDSalesResponse, Formatting.Indented), false).Wait();
            return sethistoryDSalesResponse.Contains(JsonConst.MESSAGE_FAIL);

        }
    }
}
 