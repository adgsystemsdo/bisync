﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class CxcMovItemController
    {
        //private static readonly string MESSAGE = "No hay datos para sincronizar";
        public static async Task<bool> SetCxcMovItemDiarias()
        {
            string setCxcMovHeadDiariasResponse = await CxcMovItemServices.SetCxcMovItemDiarias();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadDiariasResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadDiariasResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCxcMovItemMensual()
        {
            string setCxcMovHeadMensualResponse = await CxcMovItemServices.SetCxcMovItemMensuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadMensualResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadMensualResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCxcMovItemAnuales()
        {
            string setCxcMovHeadAnualesReponse = await CxcMovItemServices.SetCxcMovItemAnuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadAnualesReponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadAnualesReponse.Contains(JsonConst.MESSAGE_FAIL);
            
        }

        public static async Task<bool> SetCxcMovItemHistorico()
        {
            string setCxcMovItemHistoricoResponse = await CxcMovItemServices.SetCxcMovItemHistorico() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovItemHistoricoResponse, Formatting.Indented), false).Wait();
            return setCxcMovItemHistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);

        }

    }
}
