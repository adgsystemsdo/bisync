﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class DepartamentosController
    {
        public static async Task<bool> SetDepartamentos()
        {
            string setDepartamentosResponse = await DepartamentosServices.SetDepartamentos();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setDepartamentosResponse, Formatting.Indented), false).Wait();
            return setDepartamentosResponse.Contains(JsonConst.MESSAGE_FAIL) ;
            
        }
    }
}
