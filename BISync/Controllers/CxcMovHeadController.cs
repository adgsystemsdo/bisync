﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace BISync.Controllers
{
    class CxcMovHeadController
    {
        public static async Task<bool> SetCxcMovHeadDiarias()
        {
            string setCxcMovHeadDiariasResponse = await CxcMovHeadServices.SetCxcMovHeadDiaria() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadDiariasResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadDiariasResponse.Contains(JsonConst.MESSAGE_FAIL) ;

        }

        public static async Task<bool> SetCxcMovHeadMensual()
        {
            string setCxcMovHeadMensualResponse = await CxcMovHeadServices.SetCxcMovHeadMensual() ;
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadMensualResponse, Formatting.Indented), false).Wait();
            return  setCxcMovHeadMensualResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

        public static async Task<bool> SetCxcMovHeadAnuales()
        {
            string setCxcMovHeadAnualesResponse = await CxcMovHeadServices.SetCxcMovHeadAnuales();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadAnualesResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadAnualesResponse.Contains(JsonConst.MESSAGE_FAIL);

        }

        public static async Task<bool> SetCxcMovHeadHistorico()
        {
            string setCxcMovHeadhistoricoResponse = await CxcMovHeadServices.SetCxcMovHeadHistorico();
            SettingsGetter.Notify(JsonConvert.SerializeObject(setCxcMovHeadhistoricoResponse, Formatting.Indented), false).Wait();
            return setCxcMovHeadhistoricoResponse.Contains(JsonConst.MESSAGE_FAIL);
        }

    }
}
