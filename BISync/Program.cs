﻿using BISync.Controllers;
using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Globalization;
using System.Threading;

namespace BISync
{
    class Program
    {
        private static ManualResetEvent QuitEvent = new ManualResetEvent(false);
        private static JObject json;

        static void Main(string[] args)
        {
            Console.Title = typeof(Program).Name;

            //We will add some set-up stuff here later...
            Console.CancelKeyPress += (sender, eArgs) =>
            {
                QuitEvent.Set();
                eArgs.Cancel = true;
            };

            json = SettingsGetter.JsonSettings();
            Login(json);
            SettingsGetter.Notify(" ============= Start ============= ", false).Wait();
            SettingsGetter.Notify("Ambiente    : " + ((bool)json.SelectToken("Configurations.Production") ? "Producción" : "Desarrollo"), false).Wait();
            SettingsGetter.Notify("Data Base   : " + ((string)json.SelectToken("DataBase.Database")), false).Wait();

            DateTime localDate = DateTime.Now;
            string pm_am = localDate.ToString("tt", CultureInfo.InvariantCulture);

            int Elapsed = (int)json.SelectToken("Configurations.Elapsed");

            // For Interval in Hour in 24
            // This Scheduler will start at Hour:Minute and call after every Elapsed Minutes
            // IntervalInSeconds(start_hour, start_minute, minutes)
            MyScheduler.IntervalInHourAndCheck(localDate.Hour, localDate.Minute + 1, Elapsed,
            () => {
                CallDistribution.ExecuteDistribution(json).Wait();
            });
            Console.ReadLine();
        }

        private static void Login(JObject json)
        {
            UserControllers.LoginAsync(json).Wait();
        }
        
    }
}