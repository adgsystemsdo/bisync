﻿using BISync.Controllers;
using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Telegram.Bot.Types;

namespace BISync
{
    class CallDistribution
    {
        private static void Login(JObject json)
        {
            UserControllers.LoginAsync(json).Wait();
        }

        public static async Task ExecuteDistribution(JObject json)
        {
            try
            {
                bool resultDataDistribution;
                bool exec = false;

                IEnumerable<JToken> times = json.SelectTokens("$..Times[?(@.value > 0)].value");
                string day = (string)json.SelectToken("Configurations.Days");
                DateTime now = DateTime.Now;
                int hour = now.Hour;
                foreach (JToken item in times)
                {
                    if (item.ToString().Equals(hour.ToString()) && day.Contains(now.ToString("ddd")))
                        exec = true;
                }

                Message message;
                if (exec || (bool)json.SelectToken("Client.Allow"))
                {
                    Console.WriteLine("===================");
                    Console.WriteLine("In TimerCallback: " + DateTime.Now);

                    if ((bool)json.SelectToken("Telegram.Active"))
                    {
                        message = await TelegramTasks.ApiAsync();
                    }

                    if ((bool)json.SelectToken("DataSync.Sales"))
                    {
                        resultDataDistribution = await DataDistributionController.DataSaleDistributionController();
                        SettingsGetter.Notify("Redistribution of Sale Data Process..............:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    if ((bool)json.SelectToken("DataSync.Cajas"))
                    {
                        resultDataDistribution = await DataDistributionController.DataCuadreCajaDistributionController();
                        SettingsGetter.Notify("Redistribution of Cuadre Caja Data Process.......:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    if ((bool)json.SelectToken("DataSync.Cuentas_x_cobrar"))
                    {
                        resultDataDistribution = await DataDistributionController.DataCxcFacturasDistributionController();
                        SettingsGetter.Notify("Redistribution of Cxc Facturas Data Process......:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    if ((bool)json.SelectToken("DataSync.Cuentas_x_pagar"))
                    {
                        resultDataDistribution = await DataDistributionController.DataCxpFacturasDistributionController();
                        SettingsGetter.Notify("Redistribution of Cxp Facturas Data Process......:" + resultDataDistribution.ToString(), false).Wait();
                    }
                    if ((bool)json.SelectToken("DataSync.Compras"))
                    {
                        resultDataDistribution = await DataDistributionController.DataInvComprasController();
                        SettingsGetter.Notify("Redistribution of Inv Compras Data Process.......:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    if ((bool)json.SelectToken("DataSync.Devoluciones"))
                    {
                        resultDataDistribution = await DataDistributionController.DataInvDevComprasDistributionController();
                        SettingsGetter.Notify("Redistribution of Inv Dev Compras Data Process....:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    if ((bool)json.SelectToken("DataSync.Movimientos_cxc"))
                    {
                        resultDataDistribution = await DataDistributionController.DataCxcHeadMovDistributionController();
                        SettingsGetter.Notify("Redistribution of Cxc Mod Head Data Process.......:" + resultDataDistribution.ToString(), false).Wait();

                        resultDataDistribution = await DataDistributionController.DataCxcItemMovDistributionController();
                        SettingsGetter.Notify("Redistribution of CXC Mov Item Data Process.......:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    if ((bool)json.SelectToken("DataSync.Movimientos_cxp"))
                    {
                        resultDataDistribution = await DataDistributionController.DataCxpHeadMovDistributionController();
                        SettingsGetter.Notify("Redistribution of Cxp Mod Head Data Process.......:" + resultDataDistribution.ToString(), false).Wait();
                    }

                    SettingsGetter.WriteDoubleLine().Wait();

                    if (((string)json.SelectToken("WebService.token")).Length > 0)
                    {
                        bool respuesta = await DataDistributionController.DistribucionOnCloud();
                        if (!respuesta)
                            Login(json);

                        SettingsGetter.WriteDoubleLine().Wait();

                        if ((bool)json.SelectToken("DataSync.Moneda"))
                        {
                            await SysMonedaControllers.SetSysMoneda();
                            SettingsGetter.Notify("== Sys Moneda Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Auxiliares"))
                        {
                            await AuxiliariesControllers.SetAuxiliaries();
                            SettingsGetter.Notify("== Auxiliares Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Sales"))
                        {
                            await SalesControllers.SetDailySales();
                            await SalesControllers.SetMonthlySales();
                            await SalesControllers.SetYearlySales();
                            await SalesControllers.SethistorySales();

                            await SalesControllers.SetDailyDSales();
                            await SalesControllers.SetMonthlyDSales();
                            await SalesControllers.SetYearlyDSales();
                            await SalesControllers.SethistoryDSales();
                            SettingsGetter.Notify("== Ventas terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Cajas"))
                        {
                            await CuadreController.SetCuadreCajaDiario();
                            await CuadreController.SetCuadreCajaMensual();
                            await CuadreController.SetCuadreCajaAnual();
                            await CuadreController.SetCuadreCajaHistorico();
                            await InvGenDepCxcCuadresController.SetDespositoSobreCuadres();
                            SettingsGetter.Notify("== Cuadre Caja Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Cuentas_x_cobrar"))
                        {
                            await CxcFacturasControllers.SetCxcFacturaDiarias();
                            await CxcFacturasControllers.SetCxcFacturaMensual();
                            await CxcFacturasControllers.SetCxcFacturaAnuales();
                            await CxcFacturasControllers.SetCxcFacturaHistorico();
                            await CxcClientesControllers.SetCxcClientes();
                            await CxcCobradoresController.SetCxcCobrados();
                            await CxcZonasController.SetCxcZonas();

                            SettingsGetter.Notify("== Cxc Facturas Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Cuentas_x_pagar"))
                        {
                            await CxpFacturasControllers.SetCxpFacturaDiarias();
                            await CxpFacturasControllers.SetCxpFacturaMensual();
                            await CxpFacturasControllers.SetCxpFacturaAnuales();
                            await CxpFacturasControllers.SetCxpFacturaHistorico();
                            SettingsGetter.Notify("== Cxp Facturas Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Compras"))
                        {
                            await InvComprasController.SetInvComprasDiarias();
                            await InvComprasController.SetInvComprasMensuales();
                            await InvComprasController.SetInvComprasAnuales();
                            await InvComprasController.SetInvCompraHistorico();
                            SettingsGetter.Notify("== Compras Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Devoluciones"))
                        {
                            await InvDevComprasController.SetInvDevComprasDiarias();
                            await InvDevComprasController.SetInvDevComprasMensuales();
                            await InvDevComprasController.SetInvDevComprasAnuales();
                            await InvDevComprasController.SetInvDevCompraHistorico();
                            SettingsGetter.Notify("== Devolución Compras Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Nomina"))
                        {
                            await DepartamentosController.SetDepartamentos();
                            await EmpleadosController.SetEmpleados();
                            await NominaController.SetNomina();
                            await LicenciasMedicasController.SetNomLicenciaMedicas();
                            await VacacionesController.SetVacaciones();
                            await NomFamiliaresController.SetNomFamiliares();
                            SettingsGetter.Notify("== Datos de la Nomina Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Movimientos_cxc"))
                        {
                            await CxcMovHeadController.SetCxcMovHeadDiarias();
                            await CxcMovHeadController.SetCxcMovHeadMensual();
                            await CxcMovHeadController.SetCxcMovHeadAnuales();
                            await CxcMovHeadController.SetCxcMovHeadHistorico();
                            await CxcMovItemController.SetCxcMovItemDiarias();
                            await CxcMovItemController.SetCxcMovItemMensual();
                            await CxcMovItemController.SetCxcMovItemAnuales();
                            await CxcMovItemController.SetCxcMovItemHistorico();
                            SettingsGetter.Notify("== Movimientos Cuentas x Cobrar Terminado ==", false).Wait();
                        }

                        if ((bool)json.SelectToken("DataSync.Movimientos_cxp"))
                        {
                            await CxpMovHeadController.SetCxpMovHeadDiarias();
                            await CxpMovHeadController.SetCxpMovHeadMensual();
                            await CxpMovHeadController.SetCxpMovHeadAnuales();
                            await CxpMovHeadController.SetCxpMovHeadHistorico();
                            SettingsGetter.Notify("== Movimientos Cuentas x Pagar Terminado ==", false).Wait();
                        }


                        if ((bool)json.SelectToken("DataSync.Bancos"))
                        {
                            await BancosControllers.SetBancos();
                            await ConciliacionController.SetConciliaciones();
                            await TransaccionesBancosController.SetTransaccionesBancos();

                            SettingsGetter.Notify("== Movimientos Bancos Terminado ==", false).Wait();
                        }

                        SettingsGetter.Notify("Client: " + json.SelectToken("Client.Name") + " === End Process ===: " + DateTime.Now + " " + now.ToString("ddd"), false).Wait();
                    }
                    else
                    {
                        SettingsGetter.Notify("== Token defeated o invalid ==", false).Wait();
                        Login(json);
                    }
                }
                else
                {
                    Console.WriteLine("...Hold On... Cliente: " + json.SelectToken("Client.Name") + " Time: " + DateTime.Now + " " + now.ToString("ddd"));
                }
            }
            catch (Exception ex)
            {
                SettingsGetter.Notify("==== Error ====", false).Wait();
                SettingsGetter.Notify(ex.Message, false).Wait();
            }
        }
    }
}