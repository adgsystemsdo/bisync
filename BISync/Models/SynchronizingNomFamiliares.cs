﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingNomFamiliares
    {
        private static readonly string ESTADISTICAS_NOM_FAMILIARES = "nom_familiares";
        private static readonly string UPDATE_DATA_NOM_FAMILIARES = "SELECT x_success FROM estadisticas.fn_update_nom_familiares (@json, @header)";

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Familiares fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingNomFamiliares()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_NOM_FAMILIARES;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetNomFamiliares()
        {
            //SettingsGetter.Notify("Sincronizando Nom Familiares", false).Wait();           
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_NOM_FAMILIARES), "SetNomFamiliares()");
        }
    }
}