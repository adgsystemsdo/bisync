﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingDepartamento
    {
        private static readonly string ESTADISTICAS_DEPARTAMENTO = "nom_departamentos";
        private static readonly string UPDATE_DATA_DEPARTAMENTO = "SELECT x_success FROM estadisticas.fn_update_nom_departamentos (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Departamentos fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingDepartamento()
        {
            sendUpdateData.UpdateQueryData      = UPDATE_DATA_DEPARTAMENTO;
            sendUpdateData.ConfirmationMessage  = CONFIRMATION_MENSAJE;
        }

        private static DataTable GetDatosDepartamentos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetDepartamentos()
        {
            //SettingsGetter.Notify("Sincronizando Departamento ", false).Wait();
            return await sendUpdateData.Update(GetDatosDepartamentos(ESTADISTICAS_DEPARTAMENTO), "SetDepartamentos()");
        }

     }
}
