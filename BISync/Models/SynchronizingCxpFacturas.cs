﻿using System.Data;
using System.Threading.Tasks;
using BISync.Helpers;

namespace BISync.Models
{
    class SynchronizingCxpFacturas
    {
        private static readonly string ESTADISTICAS_CXP_FACTURA_DIARIAS = "cxp_factura_diarias";
        private static readonly string ESTADISTICAS_CXP_FACTURA_MENSUALES = "cxp_factura_mensuales";
        private static readonly string ESTADISTICAS_CXP_FACTURA_ANUALES = "cxp_factura_anuales";
        private static readonly string ESTADISTICAS_CXP_FACTURA_HISTORICO = "cxp_factura_historico";

        private static readonly string UPDATE_DATA_CXP_FACTURAS = "SELECT x_success FROM estadisticas.fn_update_cxp_facturas (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Cxp Facturas fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxpFacturas()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXP_FACTURAS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetCxpFacturas(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery(@"SELECT (row_number() OVER ())::integer as id,* FROM estadisticas." + tabla + " WHERE sync = false ", tabla); 
        }

        public async Task<string> SetCxpFacturasDiario()
        {
            //SettingsGetter.Notify("Sincronizando Cxp Facturas Diario ", false).Wait();
            return await sendUpdateData.Update(GetCxpFacturas(ESTADISTICAS_CXP_FACTURA_DIARIAS), "SetCxpFacturasDiario()");

        }

        public async Task<string> SetCxpFacturasMensual()
        {
            //SettingsGetter.Notify("Sincronizando Cxp Facturas Mensual ", false).Wait();
            return await sendUpdateData.Update(GetCxpFacturas(ESTADISTICAS_CXP_FACTURA_MENSUALES), "SetCxpFacturasMensual()");
        }

        public async Task<string> SetCxpFacturasAnual()
        {
            //SettingsGetter.Notify("Sincronizando Cxp Facturas Anual ", false).Wait();
            return await sendUpdateData.Update(GetCxpFacturas(ESTADISTICAS_CXP_FACTURA_ANUALES), "SetCxpFacturasAnual()");
        }

        public async Task<string> SetCxpFacturasHistorico()
        {
            //SettingsGetter.Notify("Sincronizando Cxp Facturas Historico ", false).Wait();
            return await sendUpdateData.Update(GetCxpFacturas(ESTADISTICAS_CXP_FACTURA_HISTORICO), "SetCxpFacturasHistorico()");
        }

    }

}