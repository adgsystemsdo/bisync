﻿using BISync.Helpers;
using Npgsql;
using System.Data;

namespace BISync.Models
{
    public class DataDistribution
    {
        public static readonly string FN_DISTRIBUCION_VENTA_SEGUN_FECHA_QUERY        = "select estadisticas.fn_distribucion_ventas_segun_fecha () as validated";
        public static readonly string FN_DISTRIBUCION_CUADRE_CAJA_SEGUN_FECHA_QUERY  = "select estadisticas.fn_distribucion_cuadre_caja_segun_fecha() as validated";
        public static readonly string FN_DISTRIBUCION_CXC_FACTURAS_SEGUN_FECHA_QUERY = "select estadisticas.fn_distribucion_cxc_facturas() as validated ";
        public static readonly string FN_DISTRIBUCION_CXP_FACTURAS_SEGUN_FECHA_QUERY = "select estadisticas.fn_distribucion_cxp_facturas() as validated ";
        public static readonly string FN_DISTRIBUCION_INV_COMPRAS_SEGUN_FECHA_QUERY  = "select estadisticas.fn_distribucion_inv_compra() as validated ";
        public static readonly string FN_DISTRIBUCION_INV_DEV_COMPRAS_SEGUN_FECHA_QUERY = "select estadisticas.fn_distribucion_inv_devcompras() as validated ";

        public static readonly string FN_DISTRIBUCION_CXC_MOV_HEAD_FECHA_QUERY = "select estadisticas.fn_distribucion_cxc_mov_head() as validated ";
        public static readonly string FN_DISTRIBUCION_CXC_MOV_ITEM_SEGUN_FECHA_QUERY = "select estadisticas.fn_distribucion_cxc_mov_item() as validated ";

        public static readonly string FN_DISTRIBUCION_CXP_MOV_HEAD = "select estadisticas.fn_distribucion_cxp_mov_head() as validated ";

        public static readonly string FN_DISTRICUTION = "fn_distribucion";

        public static DataTable ExecuteDataDistribution(string query)
        {
            return ExecuteDataTable.ExecuteQuery(query,"myTable");

        }
    }
}