﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingCxcCobradores
    {
        private static readonly string ESTADISTICAS_CXC_COBRADORES = "cxc_cobradores";
        private static readonly string UPDATE_DATA_CXC_COBRADORES = "select x_success from estadisticas.fn_update_cxc_cobradores (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Cobradores fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxcCobradores()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXC_COBRADORES;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetCxcCobradores()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Cobradores", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_COBRADORES), "SetCxcCobradores()");
        }
        
    }
}

