﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using Newtonsoft.Json.Linq;

namespace BISync.Models
{
    class SynchronizingEmpleado
    {
        private static readonly string ESTADISTICAS_NOM_EMPLEADOS = "nom_empleado";
        private static readonly string UPDATE_DATA_NOM_EMPLEADOS = "select x_success from estadisticas.fn_update_nom_empleado (@json, @header)";

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Empleados fueron actualizados? :  ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingEmpleado()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_NOM_EMPLEADOS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetEmpleados()
        {
            //SettingsGetter.Notify("Sincronizando Empleados ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_NOM_EMPLEADOS), "SetEmpleados()");
        }


    }
}
