﻿using BISync.Helpers;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingConciliacion
    {
        private static readonly string ESTADISTICAS_CNT_RE_CO1 = "cntreco1";
        private static readonly string UPDATE_DATA_CNT_RE_CO1 = "SELECT x_success FROM estadisticas.fn_update_cntreco1 (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Conciliación fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT(row_number() OVER())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public SynchronizingConciliacion()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CNT_RE_CO1;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public async Task<string> SetConciliaciones()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CNT_RE_CO1), "SetConciliaciones()");
        }
    }
}
