﻿using BISync.Controllers;
using BISync.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingCxcFacturas
    {
        private static readonly string ESTADISTICAS_CXC_FACTURA_DIARIAS   = "cxc_factura_diarias";
        private static readonly string ESTADISTICAS_CXC_FACTURA_MENSUALES = "cxc_factura_mensuales";
        private static readonly string ESTADISTICAS_CXC_FACTURA_ANUALES   = "cxc_factura_anuales";
        private static readonly string ESTADISTICAS_CXC_FACTURA_HISTORICO = "cxc_factura_historico";

        private static readonly string UPDATE_DATA_CXC_FACTURAS = "select x_success from estadisticas.fn_update_cxc_facturas (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE     = "Datos de Cxc Facturas fueron actualizados? :";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxcFacturas()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXC_FACTURAS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery(@"SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetCxcFacturasDiario()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Facturas Diario ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_FACTURA_DIARIAS), "SetCxcFacturasDiario()");
        }

        public async Task<string> SetCxcFacturasMensual()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Facturas Mensuales ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_FACTURA_MENSUALES), "SetCxcFacturasMensual()");

        }

        public async Task<string> SetCxcFacturasAnual()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Facturas Anual ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_FACTURA_ANUALES), "SetCxcFacturasAnual()");

        }

        public async Task<string> SetCxcFacturasHistorico()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Facturas Historico ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_FACTURA_HISTORICO), "SetCxcFacturasHistorico()");
        }

    }

}