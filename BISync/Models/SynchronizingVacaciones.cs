﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingVacaciones
    {
        private static readonly string ESTADISTICAS_VACACIONES = "nom_vacaciones";
        private static readonly string UPDATE_DATA_VACACIONES = "SELECT x_success FROM estadisticas.fn_update_nom_vacaciones (@json, @header)";

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Vacaciones fueron actualizados? :";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingVacaciones()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_VACACIONES;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatosVacaciones(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetVacaciones()
        {
            //SettingsGetter.Notify("Sincronizando Vacaciones ", false).Wait();
            return await sendUpdateData.Update(GetDatosVacaciones(ESTADISTICAS_VACACIONES), "SetVacaciones()");
        }

    }
}
