﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingDevCompras
    {
        private static readonly string ESTADISTICAS_INV_DEV_COMPRAS_DIARIAS   = "inv_devcompras_diarias";
        private static readonly string ESTADISTICAS_INV_DEV_COMPRAS_MENSUALES = "inv_devcompras_mensuales";
        private static readonly string ESTADISTICAS_INV_DEV_COMPRAS_ANUALES   = "inv_devcompras_anuales";
        private static readonly string ESTADISTICAS_INV_DEV_COMPRAS_HISTORICO = "inv_devcompras_historico";

        private static readonly string UPDATE_DATA_INV_DEV_COMPRAS  = "select x_success from estadisticas.fn_update_inv_devcompras (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE         = "Datos de INV DEV COMPRAS fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingDevCompras()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_INV_DEV_COMPRAS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetInvDevCompras(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetInvDevComprasDiarias()
        {
            //SettingsGetter.Notify("Sincronizando INV DEV COMPRAS  ", false).Wait();
            return await sendUpdateData.Update(GetInvDevCompras(ESTADISTICAS_INV_DEV_COMPRAS_DIARIAS), "SetInvDevComprasDiarias()");
        }

        public async Task<string> SetInvDevComprasMensuales()
        {
            //SettingsGetter.Notify("Sincronizando Inv Dev Compras Mensuales ", false).Wait();
            return await sendUpdateData.Update(GetInvDevCompras(ESTADISTICAS_INV_DEV_COMPRAS_MENSUALES), "SeInvDevComprasMensuales()");
        }

        public async Task<string> SetInvDevComprasAnuales()
        {
            //SettingsGetter.Notify("Sincronizando Inv Compras Anuales ", false).Wait();
            return await sendUpdateData.Update(GetInvDevCompras(ESTADISTICAS_INV_DEV_COMPRAS_ANUALES), "SetInvDevComprasAnuales()");
        }

        public async Task<string> SetInvDevComprasHistorico()
        {
            //SettingsGetter.Notify("Sincronizando Inv Compras Historico ", false).Wait();
            return await sendUpdateData.Update(GetInvDevCompras(ESTADISTICAS_INV_DEV_COMPRAS_HISTORICO), "SetInvDevComprasHistorico()");

        }


    }
}