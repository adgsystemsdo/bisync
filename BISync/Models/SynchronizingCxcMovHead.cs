﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using Newtonsoft.Json.Linq;

namespace BISync.Models
{
    class SynchronizingCxcMovHead
    {
        private static readonly string ESTADISTICAS_CXC_MOV_HEAD_DIARIAS   = "cxc_mov_head_diario";
        private static readonly string ESTADISTICAS_CXC_MOV_HEAD_MENSUALES = "cxc_mov_head_mensual";
        private static readonly string ESTADISTICAS_CXC_MOV_HEAD_ANUALES   = "cxc_mov_head_anual";
        private static readonly string ESTADISTICAS_CXC_MOV_HEAD_HISTORICO = "cxc_mov_head_historico";

        private static readonly string UPDATE_DATA_CXC_MOV_HEAD = "select x_success from estadisticas.fn_update_cxc_mov_head (@json, @header)";

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Cxc movimientos Mov Head fueron actualizados? :  ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxcMovHead()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXC_MOV_HEAD;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }


        public async Task<string> SetCxcMovHeadDiarias()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Head Diario ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_HEAD_DIARIAS), "SetCxcMovHeadDiarias()");
        }

        public async Task<string> SetCxcMovHeadMensual()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Head Mensuales ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_HEAD_MENSUALES), "SetCxcMovHeadMensual()");
        }

        public async Task<string> SetCxcMovHeadAnual()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Head Anual ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_HEAD_ANUALES), "SetCxcMovHeadAnual()");
        }

        public async Task<string> SetCxcMovHeadHistorico()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Head Historico ", false).Wait();            
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_HEAD_HISTORICO), "SetCxcMovHeadHistorico()");
        }

    }
}