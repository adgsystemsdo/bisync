﻿using BISync.Helpers;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingCxcZonas
    {
        private static readonly string ESTADISTICAS_CXC_ZONAS = "cxc_zonas";
        private static readonly string UPDATE_DATA_CXC_ZONAS  = "SELECT x_success FROM estadisticas.fn_update_cxc_zonas (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE   = "Datos de Cxc Zonas fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxcZonas()
        {
            sendUpdateData.UpdateQueryData      = UPDATE_DATA_CXC_ZONAS;
            sendUpdateData.ConfirmationMessage  = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetCxcZonas()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Zonas", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_ZONAS), "SetCxcZonas()");
        }

    }
}
