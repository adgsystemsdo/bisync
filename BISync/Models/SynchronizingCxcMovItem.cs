﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingCxcMovItem
    {
        private static readonly string ESTADISTICAS_CXC_MOV_ITEM_DIARIAS   = "cxc_mov_item_diarias";
        private static readonly string ESTADISTICAS_CXC_MOV_ITEM_MENSUALES = "cxc_mov_item_mensuales";
        private static readonly string ESTADISTICAS_CXC_MOV_ITEM_ANUALES   = "cxc_mov_item_anuales";
        private static readonly string ESTADISTICAS_CXC_MOV_ITEM_HISTORICO = "cxc_mov_item_historico";

        private static readonly string UPDATE_DATA_CXC_MOV_ITEM            = "SELECT x_success FROM estadisticas.fn_update_cxc_mov_item (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE                = "Datos de Cxc Mov Item fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxcMovItem()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXC_MOV_ITEM;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private static DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetCxcMovItemDiario()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Item Diario ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_ITEM_DIARIAS), "SetCxcItemMovDiario()");
        }

        public async Task<string> SetCxcMovItemMensual()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Item Mensuales ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_ITEM_MENSUALES), "SetCxcItemMovMensual()");

        }

        public async Task<string> SetCxcMovItemAnual()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Item Anual ", false).Wait();           
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_ITEM_ANUALES), "SetCxcItemMovAnual()");

        }

        public async Task<string> SetCxcMovItemHistorico()
        {
            //SettingsGetter.Notify("Sincronizando Cxc Mov Item Historico ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_MOV_ITEM_HISTORICO), "SetCxcItemMovHistorico()");
        }

    }
}
