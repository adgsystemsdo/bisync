﻿using BISync.Helpers;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingClientes
    {
        private static readonly string ESTADISTICAS_CXC_CLIENTES = "cxc_clientes";
        private static readonly string UPDATE_DATA_CXC_CLIENTES = "SELECT x_success FROM estadisticas.fn_update_cxc_clientes (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Cxc Clientes fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT(row_number() OVER())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public SynchronizingClientes()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXC_CLIENTES;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public async Task<string> SetClientes()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXC_CLIENTES), "SetClientes()");
        }

    }

}