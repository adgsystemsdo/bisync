﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using Newtonsoft.Json.Linq;

namespace BISync.Models
{
    class SynchronizingNomina
    {
        private static readonly string ESTADISTICAS_NOM_ENCABEZADO = "nom_encabezado";
        private static readonly string ESTADISTICAS_NOM_DETALLES   = "nom_detalle";

        private static readonly string UPDATE_DATA_NOM_ENCABEZADO  = "SELECT x_success FROM estadisticas.fn_update_nom_encabezado (@json, @header)";
        private static readonly string UPDATE_DATA_NOM_DETALLES    = "SELECT x_success FROM estadisticas.fn_update_nom_detalles (@json, @header)" ;

        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        private static DataTable GetDatosNomina(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public static async Task<string> SetEncabezadoNomina()
        {
            //SettingsGetter.Notify("Sincronizando Encabezado Nomina ", false).Wait();
            sendUpdateData.UpdateQueryData = UPDATE_DATA_NOM_ENCABEZADO;
            sendUpdateData.ConfirmationMessage = "Datos de Nomina fueron actualizados? :";
            return await sendUpdateData.Update(GetDatosNomina(ESTADISTICAS_NOM_ENCABEZADO), "SetInvDevComprasDiarias()");
        }

        public static async Task<string> SetDetalleNomina()
        {
            //SettingsGetter.Notify("Sincronizando Detalle Nomina ", false).Wait();
            sendUpdateData.UpdateQueryData = UPDATE_DATA_NOM_DETALLES;
            sendUpdateData.ConfirmationMessage = "Datos de Detalle Nomina fueron actualizados? :";
            return await sendUpdateData.Update(GetDatosNomina(ESTADISTICAS_NOM_DETALLES), "SetDetalleNomina()");

        }

    }
}
