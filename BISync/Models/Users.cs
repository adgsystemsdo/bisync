﻿using BISync.Helpers;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.IO;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Telegram.Bot.Types;

namespace BISync.Models
{
    public class Users
    {
        private static readonly HttpClient client = new HttpClient();

        static async public Task<JObject> login()
        {
            JObject jsonResponce;

            try
            {
                JObject json = SettingsGetter.JsonSettings();

                string URL = SettingsGetter.ApiLoginUrl();

                var values = new Dictionary<string, string>
                {
                   { "email", (string)json.SelectToken("WebService.email") },
                   { "password", (string)json.SelectToken("WebService.password")},
                   { "fingerprint", "zxcvbnmasdfghjklqwertyuiop"}
                };

                var httpContent = new HttpRequestMessage(HttpMethod.Post, URL);
                httpContent.Headers.ExpectContinue = false;
                httpContent.Content = new FormUrlEncodedContent(values);
                httpContent.Headers.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

                HttpResponseMessage response = await client.SendAsync(httpContent);

                if (response.IsSuccessStatusCode)
                {
                    var responseString = await response.Content.ReadAsStringAsync();

                    jsonResponce = JObject.Parse(responseString);

                    if ((string)jsonResponce.SelectToken("status") == "success")
                    {
                        updateUserToken((string)jsonResponce.SelectToken("data.token"));
                        updateClientUrl((string)jsonResponce.SelectToken("data.client.url"));

                        if ((bool)json.SelectToken("WebService.firstLogin"))
                        {
                            FirstUserLogin();
                        }
                    }
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();
                    jsonResponce = SettingsGetter.CreateJsonMessage("fail", responseString);
                }
            }
            catch(Exception ex)
            {
                jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method login()," + ex.Message);

                if ((bool)SettingsGetter.JsonSettings().SelectToken("Telegram.Active"))
                {
                    Message message;

                    message = await TelegramTasks.ApiAsyncPersonalice("Error," + ex.Message, false);
                }
                else
                {
                    Console.WriteLine("==============================================================================");
                    Console.WriteLine(ex.Message);
                }
            }

            return jsonResponce;
        }

        static public bool updateUserToken(string token)
        {
            try
            {
                string json = System.IO.File.ReadAllText((@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON)));
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                jsonObj["WebService"]["token"] = token;
                string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                System.IO.File.WriteAllText(@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON), output);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static public bool updateClientUrl(string url)
        {
            try
            {
                string json = System.IO.File.ReadAllText((@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON)));
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                jsonObj["WebService"]["clienturl"] = url;
                string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                System.IO.File.WriteAllText(@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON), output);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static public bool resetUserToken()
        {
            try
            {
                string json = System.IO.File.ReadAllText((@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON)));
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                jsonObj["WebService"]["token"] = " ";
                string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                System.IO.File.WriteAllText(@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON), output);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static public bool FirstUserLogin()
        {
            try
            {
                string json = System.IO.File.ReadAllText((@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON)));
                dynamic jsonObj = JsonConvert.DeserializeObject(json);
                jsonObj["WebService"]["firstLogin"] = "false";
                string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
                System.IO.File.WriteAllText(@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON), output);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        static async public Task<JObject> LoggedUserInfo()
        {
            JObject jsonResponce;

            try
            {
                JObject json = SettingsGetter.JsonSettings();

                string URL = SettingsGetter.ApiBaseUrl() + "users";

                var httpContent = new HttpRequestMessage(HttpMethod.Get, URL);
                httpContent.Headers.ExpectContinue = false;
                httpContent.Headers.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
                httpContent.Headers.Add("Authorization", $"Bearer {SettingsGetter.GetApiToken()}");

                HttpResponseMessage response = await client.SendAsync(httpContent);

                if (response.IsSuccessStatusCode)
                {
                    var responseString = await response.Content.ReadAsStringAsync();

                    jsonResponce = JObject.Parse(responseString);
                }
                else
                {
                    var responseString = await response.Content.ReadAsStringAsync();

                    jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method LoggedUserInfo()," + responseString);
                }
            }
            catch (Exception ex)
            {
                jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method LoggedUserInfo()," + ex.Message);

                if ((bool)SettingsGetter.JsonSettings().SelectToken("Telegram.Active"))
                {
                    Message message;
                    message = await TelegramTasks.ApiAsyncPersonalice("Error," + ex.Message, false);
                }
                else
                {
                    Console.WriteLine("==============================================================================");
                    Console.WriteLine(ex.Message);
                }
            }

            return jsonResponce;
        }
    }
}
