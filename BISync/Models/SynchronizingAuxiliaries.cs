﻿using BISync.Controllers;
using BISync.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingAuxiliaries
    {
        private static readonly string CASH_BOXES = @" SELECT cia_codigo, numcen, caja_cod as caja, caja_nom AS nombre, caja_fecha AS fecha, caja_almacen AS almacen,
                                                    CASE WHEN caja_act = '*' THEN 'ACTIVO' ELSE 'INACTIVO' END::character varying(10) as status
                                                    FROM  inv_cajas ";

        private static readonly string CASHIER = @" SELECT DISTINCt cia_codigo, caje_codigo AS cajero, caje_nombre AS nombre, 
                                                    CASE WHEN cajero_status = 'S' THEN 'ACTIVO' ELSE 'INACTIVO' END::character varying(10) AS status
                                                    FROM inv_cajeros";

        private static readonly string STORAGES = @"SELECT DISTINCT cia_codigo, alma_codigo AS almacen, alma_nombre AS nombre, 
                                                    coalesce(to_char(alma_apertura,'yyyy-MM-dd'), '2014-01-01'::text)::date AS fecha, 
                                                    CASE WHEN alma_status = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' end::character varying(10) AS status
                                                    FROM inv_almacenes 
                                                    WHERE COALESCE(cia_codigo, '') <> '' ";

        private static readonly string COST_CENTER = @"SELECT DISTINCT cia_codigo, numecen AS numcen, desccen AS nombre, encacen AS encargado, fcrecen AS fecha, centro_direccion AS direccion, 
                                                    centro_telefonos AS telefonos, CASE WHEN estacen = 'A' THEN 'ACTIVO' ELSE 'INACTIVO' END::character varying(10) AS status
                                                    FROM cntrecen ";

        private static readonly string SALES_MEN = @" SELECT DISTINCT cia_codigo, vend_codigo AS vendedor, vend_nombre AS nombre, vend_telefono AS telefono, 
                                                    CASE WHEN usua_fecha > '19800101' THEN to_timestamp(usua_fecha::text, 'dd-MM-YYYY HH:MI:SSSS PM') ELSE
                                                    to_timestamp('01-01-1980'::text, 'dd-MM-YYYY HH:MI:SSSS PM') END AS fecha, 
                                                    CASE WHEN vend_estado = '1' THEN 'ACTIVO' ELSE 'INACTIVO' END::character varying(10) AS status
                                                    FROM cxc_vendedores ";

        private static readonly string DEBT_COLLECTOR = @" SELECT DISTINCT cia_codigo, cobr_codigo AS cobrador, cobr_nombre AS nombre, cobr_telefono AS telefono,
                                                        CASE WHEN usua_fecha > '19800101' THEN to_timestamp(usua_fecha::text, 'dd-MM-YYYY HH:MI:SSSS PM') ELSE
                                                        to_timestamp('01-01-1980'::text, 'dd-MM-YYYY HH:MI:SSSS PM') END AS fecha,
                                                        CASE WHEN cobr_estado = '1' THEN 'ACTIVO' ELSE 'INACTIVO' END::character varying(10) AS status
                                                        FROM cxc_cobradores";

        private static readonly string CLASIFICATIONS = " SELECT cia_codigo, depa_codigo AS codigo, depa_nombre AS nombre FROM inv_departamentos";

        private static readonly string SUB_CLASIFICATIONS = @" SELECT s.cia_codigo, s.depa_codigo as dcodigo, s.sdepa_codigo as codigo, d.depa_nombre as dnombre, s.sdepa_nombre as nombre
                                                            FROM inv_sub_departamentos s
                                                            INNER JOIN  inv_departamentos d ON (d.depa_codigo = s.depa_codigo and d.cia_codigo = s.cia_codigo)  ";

        private static readonly string FAMILIES = " SELECT s.cia_codigo, s.clave_codigo as codigo, s.clave_nombre as nombre, s.codigo as secuencia FROM inv_claves s ";

        private static readonly string TIPOS_NOMINAS = " SELECT cia_codigo, id, tipo_codigo, tipo_descripcion, tipo_secuencia, tipo_frecuencia FROM nom.tipos_nominas ";

        private static DataTable GetDataAuxiliares(string query, string tableName)
        {
            return ExecuteDataTable.ExecuteQuery(query, tableName);
        }

        public static DataSet GetAuxiliaries()
        {
            DataSet ds = new DataSet();
            
            DataTable cashBoxesTable = new DataTable();
            DataTable cashierTable = new DataTable();
            DataTable storagesTable = new DataTable();
            DataTable costCenterTable = new DataTable();
            DataTable debtCollectorTable = new DataTable();
            DataTable calificationTable = new DataTable();
            DataTable subCalifications = new DataTable();
            DataTable salesMenTable = new DataTable();
            DataTable familiesTable = new DataTable();
            DataTable tiposNominaTable = new DataTable();

            cashBoxesTable = GetDataAuxiliares(CASH_BOXES, "cajas");
            cashierTable = GetDataAuxiliares(CASHIER, "cajeros");
            storagesTable = GetDataAuxiliares(STORAGES, "almacenes");
            costCenterTable = GetDataAuxiliares(COST_CENTER, "centros_costos");
            debtCollectorTable = GetDataAuxiliares(DEBT_COLLECTOR, "cobradores");
            calificationTable = GetDataAuxiliares(CLASIFICATIONS, "departamento");
            subCalifications = GetDataAuxiliares(SUB_CLASIFICATIONS, "subdepartamento");
            salesMenTable = GetDataAuxiliares(SALES_MEN, "vendedores");
            familiesTable = GetDataAuxiliares(FAMILIES, "families");
            tiposNominaTable = GetDataAuxiliares(TIPOS_NOMINAS, "tipos_nominas");

            ds.Tables.Add(cashBoxesTable);
            ds.Tables.Add(cashierTable);
            ds.Tables.Add(storagesTable);
            ds.Tables.Add(costCenterTable);
            ds.Tables.Add(debtCollectorTable);
            ds.Tables.Add(calificationTable);
            ds.Tables.Add(subCalifications);
            ds.Tables.Add(salesMenTable);
            ds.Tables.Add(familiesTable);
            ds.Tables.Add(tiposNominaTable);

            DataTable table = ds.Tables["cajas"];

            if (table.Rows.Count <= 0)
            ds.Tables.Remove(table);

            table = ds.Tables["cajeros"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["almacenes"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["centros_costos"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["cobradores"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["departamento"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["subdepartamento"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["vendedores"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["families"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            table = ds.Tables["tipos_nominas"];
            if (table.Rows.Count <= 0)
                ds.Tables.Remove(table);

            return ds;
        }

        public static async Task<JObject> SetAuxiliaries()
        {
            return await SetSendAuxiliaries(GetAuxiliaries(), "auxiliares", "setAuxiliaries()");
        }

        public static async Task<JObject> SetSendAuxiliaries(DataSet ds, string method, string process)
        {
            JObject jsonResponce;

            if (ds.Tables.Count > 0)
            {
                string URL = SettingsGetter.ApiClientUrl() + method;

                decimal count = ((decimal)100 / (decimal)ds.Tables.Count);
                decimal progress = 0;
                string json = "";

                jsonResponce = SettingsGetter.CreateJsonMessage(" ", " ");

                SettingsGetter.Notify("0% -> 100%: " + DateTime.Now, false).Wait();

                foreach (DataTable table in ds.Tables)
                {
                    json += "'" + table.TableName + "': " + JsonConvert.SerializeObject(table, Formatting.Indented) + "";

                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    progress = (progress + count);
                    SettingsGetter.Notify(progress.ToString("0.##") + "% -> 100%: " + DateTime.Now, true).Wait();
                    Console.SetCursorPosition(0, Console.CursorTop);

                    if (progress < 100) { json += "," + Environment.NewLine; } else { json += ""; };
                }

                string data = @"{ '" + method + "' : { " + json + "}}";
                data = data.Replace((char)39, (char)34);

                jsonResponce = await SendAndUpdateData.SendAuxiliaresData (data, URL, process);

                if ((string)jsonResponce.SelectToken("status") == "fail" && (string)jsonResponce.SelectToken("message") == "Token expirada")
                {
                    bool result = await UserControllers.LoginAsync(SettingsGetter.JsonSettings());

                    if (result)
                    {
                        jsonResponce = await SendAndUpdateData.SendAuxiliaresData(@"{ '" + method + "' : { " + json + "}}", URL, process);
                    }
                }
                else if ((string)jsonResponce.SelectToken("status") == "fail" && (string)jsonResponce.SelectToken("message") == "No autorizado")
                {
                    bool result = await UserControllers.LoginAsync(SettingsGetter.JsonSettings());

                    if (result)
                    {
                        jsonResponce = await SendAndUpdateData.SendAuxiliaresData(@"{ '" + method + "' : { " + json + "}}", URL, process);
                    }
                }
                else if ((string)jsonResponce.SelectToken("status") != "success")
                {
                    return jsonResponce;
                }
            }
            else
            {
                jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method: " + process + ", No hay datos para sincronizar");
            }

            return jsonResponce;
        }
    }

}