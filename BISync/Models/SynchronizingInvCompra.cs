﻿using BISync.Controllers;
using BISync.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingInvCompra
    {
        private static readonly string INV_COMPRAS_DIARIAS = "inv_compras_diarias";
        private static readonly string INV_COMPRAS_MENSAULES = "inv_compras_mensuales";
        private static readonly string INV_COMPRAS_ANUALES = "inv_compras_anuales";
        private static readonly string INV_COMPRAS_HISTORICO = "inv_compra_historico";

        private static readonly string UPDATE_DATA_INV_COMPRAS = "SELECT x_success from estadisticas.fn_update_inv_compras (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE= "Datos de Inv Compras fueron actualizados ? : ";

        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingInvCompra()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_INV_COMPRAS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;            
        }

        private DataTable GetInvCompras(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetInvComprasDiarias()
        {
            //SettingsGetter.Notify("Sincronizando InvCompras Diarias ", false).Wait();
            return await sendUpdateData.Update(GetInvCompras(INV_COMPRAS_DIARIAS), "SetInvComprasDiarias()");
       }

        public async Task<string> SetInvComprasMensuales()
        {
            //SettingsGetter.Notify("Sincronizando InvCompras MENSUALES ", false).Wait();           
            return await sendUpdateData.Update(GetInvCompras(INV_COMPRAS_MENSAULES), "SetInvComprasMensuales()");
        }

        public async Task<string> SetInvComprasAnuales()
        {
            //SettingsGetter.Notify("Sincronizando InvCompras Anuales ", false).Wait();
            return await sendUpdateData.Update(GetInvCompras(INV_COMPRAS_ANUALES), "SetInvComprasAnuales()");
        }

        public async Task<string> SetInvCompraHistorico()
        {
            //SettingsGetter.Notify("Sincronizando InvCompras Historico ", false).Wait();            
            return await sendUpdateData.Update (GetInvCompras(INV_COMPRAS_HISTORICO), "SetInvCompraHistorico()");
        }


    }
}
 