﻿using BISync.Helpers;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingTransaccionesBancos
    {
        private static readonly string ESTADISTICAS_CNT_RE_TRA = "cntretra";
        private static readonly string UPDATE_DATA_CNT_RE_TRA = "SELECT x_success FROM estadisticas.fn_update_cntretra (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de las Transacciones fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT(row_number() OVER())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public SynchronizingTransaccionesBancos()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CNT_RE_TRA;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public async Task<string> SetTransacciones()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CNT_RE_TRA), "SetTransacciones()");
        }

    }
}