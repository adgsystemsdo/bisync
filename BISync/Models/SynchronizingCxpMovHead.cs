﻿using System.Data;
using System.Threading.Tasks;
using BISync.Helpers;
namespace BISync.Models
{
    class SynchronizingCxpMovHead
    {
        private static readonly string ESTADISTICAS_CXP_MOV_HEAD_DIARIA    = "cxp_mov_head_diario";
        private static readonly string ESTADISTICAS_CXP_MOV_HEAD_MENSUAL   = "cxp_mov_head_mensual";
        private static readonly string ESTADISTICAS_CXP_MOV_HEAD_ANUAL     = "cxp_mov_head_anual";
        private static readonly string ESTADISTICAS_CXP_MOV_HEAD_HISTORICO = "cxp_mov_head_historico";

        private static readonly string UPDATE_DATA_CXP_MOV_HEAD = "select x_success from estadisticas.fn_update_cxp_mov_head (@json, @header)";

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Cxp movimientos Mov Head fueron actualizados? :  ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCxpMovHead()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CXP_MOV_HEAD;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public async Task<string> SetCxPMovHeadDiarias()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXP_MOV_HEAD_DIARIA), "SetCxPMovHeadDiarias()");
        }

        public async Task<string> SetCxpMovHeadMensual()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXP_MOV_HEAD_MENSUAL), "SetCxpMovHeadMensual()");
        }

        public async Task<string> SetCxpMovHeadAnual()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXP_MOV_HEAD_ANUAL), "SetCxpMovHeadAnual()");
        }

        public async Task<string> SetCxpMovHeadHistorico()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CXP_MOV_HEAD_HISTORICO), "SetCxpMovHeadHistorico()");
        }

    }
}