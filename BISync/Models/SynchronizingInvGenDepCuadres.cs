﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingInvGenDepCuadres
    {
        private static readonly string ESTADISTICAS_DEPOSITOS_CUADRE = "inv_gendepcxc_cuadres";
        private static readonly string UPDATE_DATA_DEPOSITOS = "Select x_success from  estadisticas.fn_update_inv_gendepcxc(@json, @header)";

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Depositos sobre Cuadres fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingInvGenDepCuadres()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_DEPOSITOS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private static DataTable GetDatosGenCuadres(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetDepCuadres()
        {
            //SettingsGetter.Notify("Sincronizando Depositos de cuadres ", false).Wait();
            return await sendUpdateData.Update(GetDatosGenCuadres(ESTADISTICAS_DEPOSITOS_CUADRE), "SetDepCuadre()");
        }
    }

}
