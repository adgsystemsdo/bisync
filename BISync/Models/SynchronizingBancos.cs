﻿using BISync.Helpers;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingBancos
    {
        private static readonly string ESTADISTICAS_CNT_RE_BAT= "cntrebat";
        private static readonly string UPDATE_DATA_CNT_RE_BAT = "SELECT x_success FROM estadisticas.fn_update_cntrebat (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Bancos fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT(row_number() OVER())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public SynchronizingBancos()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_CNT_RE_BAT;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public async Task<string> SetBancos()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_CNT_RE_BAT), "SetBancos()");
        }

    }
}
