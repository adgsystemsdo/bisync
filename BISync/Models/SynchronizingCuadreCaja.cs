﻿using BISync.Controllers;
using BISync.Helpers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingCuadreCaja
    {

        private static readonly string ESTADISTICAS_INV_CUADRE_DIARIO = "cuadre_caja_diario";
        private static readonly string ESTADISTICAS_INV_CUADRE_MENSUAL = "cuadre_caja_mensual";
        private static readonly string ESTADISTICAS_INV_CUADRE_ANUAL = "cuadre_caja_anual";
        private static readonly string ESTADISTICAS_INV_CUADRE_HISTORICO = "cuadre_caja_historico";

        private static readonly string UPDATE_DATA_INV_CUADRE = "select x_success from estadisticas.fn_update_cuadre_caja (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE = "Datos de Inv Cuadres fueron actualizados? :";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingCuadreCaja()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_INV_CUADRE;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public  DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetCuadreCajaDiario()
        {
            //SettingsGetter.Notify("Sincronizando Cuadre Caja Diario ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_INV_CUADRE_DIARIO), "SetCuadreCajaDiario()");
        }

        public async Task<string> SetCuadreCajaMensual()
        {
            //SettingsGetter.Notify("Sincronizando Cuadre Caja Mensual", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_INV_CUADRE_MENSUAL), "SetCuadreCajaMensual()");
        }

        public async Task<string> SetCuadreCajaAnual()
        {
            //SettingsGetter.Notify("Sincronizando Cuadre Caja Anual", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_INV_CUADRE_ANUAL), "SetCuadreCajaAnual()");
        }

        public async Task<string> SetCuadreCajaHistorico()
        {
            //SettingsGetter.Notify("Sincronizando Cuadre Caja Historico ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_INV_CUADRE_HISTORICO), "SetCuadreCajaHistorico()");
        }

    }
}