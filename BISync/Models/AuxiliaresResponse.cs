﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace BISync.Models
{
    class AuxiliaresResponse
    {
        [JsonProperty(PropertyName = "status")]
        private string Status { get; set; }

        [JsonProperty(PropertyName = "message")]
        private string Message { get; set; }

    }
}