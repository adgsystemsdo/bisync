﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    public class SynchronizingSales
    {
        private static readonly string ESTADISTICAS_SALES_DIARIAS = "ventas_diarias";
        private static readonly string ESTADISTICAS_SALES_MENSUALES = "ventas_mensuales";
        private static readonly string ESTADISTICAS_SALES_ANUALES = "ventas_anuales";
        private static readonly string ESTADISTICAS_SALES_HISTORICO = "ventas_historico";

        private static readonly string ESTADISTICAS_SALES_D_DIARIAS = "ventas_d_diarias";
        private static readonly string ESTADISTICAS_SALES_D_MENSUALES = "ventas_d_mensuales";
        private static readonly string ESTADISTICAS_SALES_D_ANUALES = "ventas_d_anuales";
        private static readonly string ESTADISTICAS_SALES_D_HISTORICO = "ventas_d_historico";

        private static readonly string UPDATE_D_SALES = "SELECT x_success FROM estadisticas.fn_update_d_sales (@json, @header)";
        private static readonly string UPDATE_SALES = "SELECT x_success FROM estadisticas.fn_update_sales (@json, @header)";

        private static readonly SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        private static JObject json = SettingsGetter.JsonSettings();

        private static DataTable GetDataSales(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false limit 50000", tabla);
        }

        private static DataTable GetDataSalesD(string tabla)
        {
            string query = @"SELECT (row_number() OVER ())::integer as id, fecha, year, mes, cia_codigo, numcen, codigo, year_cen, mes_cen, 
                                       referencia, nombre, precio, preciov, cantidad, costo, costo_total, 
                                       itbis, descuento, porciento_descuento, importe_bruto, importe_monto, 
                                       importe_descontado, importe_neto, tiene_descuento, dcodigo, departamento, 
                                       sdcodigo, subdepartamento, acodigo, almacen, tipo_itbis, porciento_itbis, 
                                       idmarca, marca, idfamilia, familia, idunidad, unidad, tipo, lote, 
                                       serial, importado, item_serial, rebajar, unidad_base, sync
					            FROM estadisticas." + tabla + @"
                                WHERE sync = false
					            GROUP BY fecha, year, mes, cia_codigo, numcen, codigo, year_cen, mes_cen, 
                                       referencia, nombre, precio, preciov, cantidad, costo, costo_total, 
                                       itbis, descuento, porciento_descuento, importe_bruto, importe_monto, 
                                       importe_descontado, importe_neto, tiene_descuento, dcodigo, departamento, 
                                       sdcodigo, subdepartamento, acodigo, almacen, tipo_itbis, porciento_itbis, 
                                       idmarca, marca, idfamilia, familia, idunidad, unidad, tipo, lote, 
                                       serial, importado, item_serial, rebajar, unidad_base, sync limit 50000";

            return ExecuteDataTable.ExecuteQuery(query, tabla);

        }
        
        public static async Task<string> SetDailySales()
        {
            sendUpdateData.UpdateQueryData = UPDATE_SALES;
            return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_DIARIAS), "SetDailySales()");

        }

        public static async Task<string> SetMonthlySales()
        {
            sendUpdateData.UpdateQueryData = UPDATE_SALES;
            return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_MENSUALES), "SetMonthlySales()");
        }

        public static async Task<string> SetYearlySales()
        {
            sendUpdateData.UpdateQueryData = UPDATE_SALES;
            return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_ANUALES), "SetYearlySales()");
        }

        public static async Task<string> SethistorySales()
        {
            if ((bool)json.SelectToken("DataSync.SaleHistory"))
            {
                sendUpdateData.UpdateQueryData = UPDATE_SALES;
                return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_HISTORICO), "SethistorySales()");
            }
            else
            {
                return SettingsGetter.CreateJsonMessage("fail", "Method: SethistorySales(), No esta activa la sincronizacion del historico").ToString();
            }

        }

        public static async Task<string> SetDailyDSales()
        {

            sendUpdateData.UpdateQueryData = UPDATE_D_SALES;
            return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_D_DIARIAS), "SetDailyDSales()");


        }

        public static async Task<string> SetMonthlyDSales()
        {

            sendUpdateData.UpdateQueryData = UPDATE_D_SALES;
            return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_D_MENSUALES), "setMonthlySales()");

        }

        public static async Task<string> SetYearlyDSales()
        {
            sendUpdateData.UpdateQueryData = UPDATE_D_SALES;
            return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_D_ANUALES), "SetYearlyDSales()");

        }

        public static async Task<string> SethistoryDSales()
        {

            JObject json = SettingsGetter.JsonSettings();
            if ((bool)json.SelectToken("DataSync.SaleDHistory"))
            {
                sendUpdateData.UpdateQueryData = UPDATE_D_SALES;
                return await sendUpdateData.Update(GetDataSales(ESTADISTICAS_SALES_D_HISTORICO), "SethistoryDSales()");
            }
            else
            {
                return SettingsGetter.CreateJsonMessage("fail", "Method: sethistoryDSales(), No esta activa la sincronizacion del detalle del historico").ToString();
            }

        }        
    }
}