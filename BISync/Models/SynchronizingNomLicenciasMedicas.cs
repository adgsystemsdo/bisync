﻿using BISync.Helpers;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingNomLicenciasMedicas
    {
        private static readonly string ESTADISTICAS_NOM_LICENCIA_MEDICA = "nom_licencias_medicas";
        private static readonly string UPDATE_DATA_NOM_LICENCIA_MEDICA = "SELECT x_success FROM estadisticas.fn_update_nom_licencias_medicas(@json, @header)";

        private static readonly string CONFIRMATION_MENSAJE = "Datos de Licencias Medicas fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData = new SendAndUpdateData();

        public SynchronizingNomLicenciasMedicas()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_NOM_LICENCIA_MEDICA;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT (row_number() OVER ())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public async Task<string> SetNomLicenciaMedica()
        {
            //SettingsGetter.Notify("Sincronizando Nom Licencia Medica ", false).Wait();
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_NOM_LICENCIA_MEDICA), "SetNomLicenciaMedica()");
        }

    }
}
