﻿using BISync.Helpers;
using System.Data;
using System.Threading.Tasks;

namespace BISync.Models
{
    class SynchronizingSisMoneda
    {
        private static readonly string ESTADISTICAS_SIS_MONEDAS = "sis_monedas";
        private static readonly string UPDATE_DATA_SIS_MONEDAS  = "SELECT x_success FROM estadisticas.fn_update_sis_monedas (@json, @header)";
        private static readonly string CONFIRMATION_MENSAJE     = "Datos de Monedas fueron actualizados? : ";
        private static SendAndUpdateData sendUpdateData         = new SendAndUpdateData();

        private DataTable GetDatos(string tabla)
        {
            return ExecuteDataTable.ExecuteQuery("SELECT(row_number() OVER())::integer as id, * FROM estadisticas." + tabla + " WHERE sync = false", tabla);
        }

        public SynchronizingSisMoneda()
        {
            sendUpdateData.UpdateQueryData = UPDATE_DATA_SIS_MONEDAS;
            sendUpdateData.ConfirmationMessage = CONFIRMATION_MENSAJE;
        }

        public async Task<string> SetSysMonedas()
        {
            return await sendUpdateData.Update(GetDatos(ESTADISTICAS_SIS_MONEDAS), "SetSysMonedas()");
        }

    }
}
