﻿using BISync.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Services
{
    class InvGenDepCxcCuadresServices
    {
        static SynchronizingInvGenDepCuadres synchronizingDepositosSobreCuadres = new SynchronizingInvGenDepCuadres();

        public static async Task<string> SetDepositosSobreCuadres()
        {
            return await synchronizingDepositosSobreCuadres.SetDepCuadres();
        }

    }
}
