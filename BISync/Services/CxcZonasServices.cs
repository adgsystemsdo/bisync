﻿using BISync.Models;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BISync.Services
{
    class CxcZonasServices
    {
        static SynchronizingCxcZonas synchronizingCxcZonas = new SynchronizingCxcZonas();
        public static async Task<string> SetCxcZonas()
        {
            return await synchronizingCxcZonas.SetCxcZonas();
        }

    }
}
