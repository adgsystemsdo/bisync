﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class CxcMovItemServices
    {
        static SynchronizingCxcMovItem synchronizingCxcMovItem = new SynchronizingCxcMovItem();

        public static async Task<string> SetCxcMovItemDiarias()
        {
            return await synchronizingCxcMovItem.SetCxcMovItemDiario();
        }

        public static async Task<string> SetCxcMovItemMensuales()
        {
            return await synchronizingCxcMovItem.SetCxcMovItemMensual();
        }

        public static async Task<string> SetCxcMovItemAnuales()
        {
            return await synchronizingCxcMovItem.SetCxcMovItemAnual();
        }

        public static async Task<string> SetCxcMovItemHistorico()
        {
            return await synchronizingCxcMovItem.SetCxcMovItemHistorico();
        }


    }
}
