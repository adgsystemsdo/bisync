﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;
using Newtonsoft.Json.Linq;

namespace BISync.Services
{
    class VacacionesServices
    {
        static SynchronizingVacaciones synchronizingVacaciones = new SynchronizingVacaciones();

        public static async Task<string> SetVacaciones()
        {
            return await synchronizingVacaciones.SetVacaciones();
        }
    }
}
