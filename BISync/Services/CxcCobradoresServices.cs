﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;
using Newtonsoft.Json.Linq;

namespace BISync.Services
{
    class CxcCobradoresServices
    {
        private static SynchronizingCxcCobradores synchronizingCxcCobradores = new SynchronizingCxcCobradores();
        public static async Task<string> SetCxcCobradores()
        {
            return await synchronizingCxcCobradores.SetCxcCobradores();
        }
    }
}
