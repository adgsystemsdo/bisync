﻿using System.Threading.Tasks;
using BISync.Models ;
namespace BISync.Services
{
    class CxcFacturasServices
    {
        static SynchronizingCxcFacturas  synchronizingCxcFacturas = new SynchronizingCxcFacturas();

        public static async Task<string> SetCxcFacturaDiaria()
        {
            return await synchronizingCxcFacturas.SetCxcFacturasDiario();
        }

        public static async Task<string> SetCxcFacturaMensual()
        {
            return await synchronizingCxcFacturas.SetCxcFacturasMensual();
        }

        public static async Task<string> SetCxcFacturasAnuales()
        {
            return await synchronizingCxcFacturas.SetCxcFacturasAnual();
        }

        public static async Task<string> SetCxcFacturashistorico()
        {
            return await synchronizingCxcFacturas.SetCxcFacturasHistorico();
        }
    }
}
