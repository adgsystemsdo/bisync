﻿using BISync.Models;
using Newtonsoft.Json.Linq;
using System.Threading.Tasks;

namespace BISync.Services
{
    public class SalesServices
    {
        public static async Task<string> SetDailySales()
        {
            return await SynchronizingSales.SetDailySales();
        }

        public static async Task<string> SetMonthlySales()
        {
            return await SynchronizingSales.SetMonthlySales();
        }

        public static async Task<string> SetYearlySales()
        {
            return await SynchronizingSales.SetYearlySales();
        }

        public static async Task<string> SethistorySales()
        {
            return await SynchronizingSales.SethistorySales();
        }

        public static async Task<string> SetDailyDSales()
        {
            return await SynchronizingSales.SetDailyDSales();
        }

        public static async Task<string> SetMonthlyDSales()
        {
            return await SynchronizingSales.SetMonthlyDSales();
        }

        public static async Task<string> SetYearlyDSales()
        {
            return await SynchronizingSales.SetYearlyDSales();
        }

        public static async Task<string> SethistoryDSales()
        {
            return await SynchronizingSales.SethistoryDSales();
        }
    }
}
