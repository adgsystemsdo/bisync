﻿using BISync.Models;
using System.Threading.Tasks;

namespace BISync.Services
{
    class BancosServices
    {
        static SynchronizingBancos synchronizingBancos = new SynchronizingBancos();
        public static async Task<string> SetBancos()
        {
            return await synchronizingBancos.SetBancos();
        }
    }
}
