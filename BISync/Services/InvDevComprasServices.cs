﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class InvDevComprasServices
    {
        static SynchronizingDevCompras synchronizingDevCompra = new SynchronizingDevCompras();

        public static async Task<string> SetInvDevComprasDiarias()
        {
            return await synchronizingDevCompra.SetInvDevComprasDiarias();
        }

        public static async Task<string> SetInvDevComprasMensuales()
        {
            return await synchronizingDevCompra.SetInvDevComprasMensuales();
        }

        public static async Task<string> SetInvDevComprasAnuales()
        {
            return await synchronizingDevCompra.SetInvDevComprasAnuales();
        }

        public static async Task<string> SetInvDevCompraHistorial()
        {
            return await synchronizingDevCompra.SetInvDevComprasHistorico();
        }


    }
}
