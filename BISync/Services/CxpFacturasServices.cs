﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class CxpFacturasServices
    {
        static SynchronizingCxpFacturas synchronizinigCxpFacturas = new SynchronizingCxpFacturas();

        public static async Task<string> SetCxpFacturaDiaria()
        {
            return await synchronizinigCxpFacturas.SetCxpFacturasDiario(); 
        }

        public static async Task<string> SetCxpFacturaMensual()
        {
             return await synchronizinigCxpFacturas.SetCxpFacturasMensual();
        }

        public static async Task<string> SetCxpFacturasAnuales()
        {
            return await synchronizinigCxpFacturas.SetCxpFacturasAnual();
        }

        public static async Task<string> SetCxpFacturashistorico()
        {
            return await synchronizinigCxpFacturas.SetCxpFacturasHistorico();
        }
    }
}
