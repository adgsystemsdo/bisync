﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class CuadreCajaServices
    {
        static SynchronizingCuadreCaja synchronizingCuadreCaja = new SynchronizingCuadreCaja();

        public static async Task<string> SetCuadreCajaDiario()
        {
            return await synchronizingCuadreCaja.SetCuadreCajaDiario();
        }

        public static async Task<string> SetCuadreCajaMensual()
        {
            return await synchronizingCuadreCaja.SetCuadreCajaMensual();
        }

        public static async Task<string> SetCuadreCajaAnual()
        {
            return await synchronizingCuadreCaja.SetCuadreCajaAnual();
        }

        public static async Task<string> SetCuadreCajahistorico()
        {
            return await synchronizingCuadreCaja.SetCuadreCajaHistorico();
        }
    }
}
