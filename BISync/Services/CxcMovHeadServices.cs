﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class CxcMovHeadServices
    {
        static SynchronizingCxcMovHead synchronizingCxcMovHead = new SynchronizingCxcMovHead();

        public static async Task<string> SetCxcMovHeadDiaria()
        {
            return await synchronizingCxcMovHead.SetCxcMovHeadDiarias();
        }

        public static async Task<string> SetCxcMovHeadMensual()
        {
            return await synchronizingCxcMovHead.SetCxcMovHeadMensual();
        }

        public static async Task<string> SetCxcMovHeadAnuales()
        {
            return await synchronizingCxcMovHead.SetCxcMovHeadAnual();
        }

        public static async Task<string> SetCxcMovHeadHistorico()
        {
            return await synchronizingCxcMovHead.SetCxcMovHeadHistorico();
        }

    }
}
