﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;
using Newtonsoft.Json.Linq;

namespace BISync.Services
{
    class LicenciasMedicasServices
    {
        static SynchronizingNomLicenciasMedicas synchronizingNomLicencuia = new SynchronizingNomLicenciasMedicas();

        public static async Task<string> SetNomLicenciaMedicas()
        {
            return await synchronizingNomLicencuia.SetNomLicenciaMedica();
        }
    }
}
