﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;
using Newtonsoft.Json.Linq;

namespace BISync.Services
{
    class NominaServices
    {
        public static async Task<string> SetDetalleNomina()
        {
            return await SynchronizingNomina.SetDetalleNomina();            
        }

        public static async Task<string> SetEncabezadoNomina()
        {
            return await SynchronizingNomina.SetEncabezadoNomina();
        }
    }
}