﻿using System;
using System.Threading.Tasks;
using BISync.Helpers;
using BISync.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Telegram.Bot.Types;

namespace BISync.Services
{
    public class UserServices
    {
        public static async Task<JObject> Login()
        {
            return await Users.login();
        }

        static public bool resetUserToken()
        {
            return Users.resetUserToken();
        }

        static public async Task<JObject> LoggedUserInfo()
        {
            return await Users.LoggedUserInfo();
        }

        static public async Task<bool> LoginAsync(JObject Json)
        {
            try
            {
                Message message;
                JObject login = await Login();

                if ((string)login.SelectToken("status") != "success")
                {
                    string sms = "Login attempt fail with:" + JsonConvert.SerializeObject(login, Formatting.Indented);
                    string sms2 = "Reseting Access Token!!!";
                    string sms3 = resetUserToken() ? "Access Token Reseted" : "Faild to Reset Access Token";
                    SettingsGetter.Notify("Login Fail", false).Wait();

                    if ((bool)Json.SelectToken("Telegram.Active"))
                    {
                        message = await TelegramTasks.ApiAsyncPersonalice(sms, false);
                        message = await TelegramTasks.ApiAsyncPersonalice(sms2, false);
                        message = await TelegramTasks.ApiAsyncPersonalice(sms3, false);
                    }
                    else
                    {
                        Console.WriteLine(sms);
                        Console.WriteLine(sms2);
                        Console.WriteLine(sms3);
                    }

                    return false;
                }
                else
                {
                    SettingsGetter.Notify("Login Successful!!!", false).Wait();
                    //SettingsGetter.Notify(JsonConvert.SerializeObject(login, Formatting.Indented), false).Wait();
                    return true;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
