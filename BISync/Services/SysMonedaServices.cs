﻿using BISync.Models;
using System.Threading.Tasks;

namespace BISync.Services
{
    class SysMonedaServices
    {
        static SynchronizingSisMoneda synchronizingMonedas = new SynchronizingSisMoneda();

        public static async Task<string> SetSysMonedas()
        {
            return await synchronizingMonedas.SetSysMonedas();
        }
    }
}
