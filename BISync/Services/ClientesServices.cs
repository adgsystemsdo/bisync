﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class ClientesServices
    {
        static SynchronizingClientes synchronizingClientes = new SynchronizingClientes();
        public static async Task<string> SetClientes()
        {
            return await synchronizingClientes.SetClientes();
        }
    }
}
