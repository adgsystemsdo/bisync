﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class InvComprasServices
    {
        static SynchronizingInvCompra synchronizingDevCompra = new SynchronizingInvCompra();

        public static async Task<string> SetInvComprasDiarias()
        {
            return await synchronizingDevCompra.SetInvComprasDiarias();
        }

        public static async Task<string> SetInvComprasMensuales()
        {
            return await synchronizingDevCompra.SetInvComprasMensuales(); 
        }

        public static async Task<string> SetInvComprasAnuales()
        {
            return await synchronizingDevCompra.SetInvComprasAnuales() ;
        }

        public static async Task<string> SetInvCompraHistorial()
        {
            return await synchronizingDevCompra.SetInvCompraHistorico();
        }

    }
}