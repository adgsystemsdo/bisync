﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;
using Newtonsoft.Json.Linq;

namespace BISync.Services
{
    class EmpleadosServices
    {
        static SynchronizingEmpleado synchronizingEmpleado = new SynchronizingEmpleado();

        public static async Task<string> SetEmpleados()
        {
            return await synchronizingEmpleado.SetEmpleados();
        }
    }
}
