﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class DepartamentosServices
    {
        static SynchronizingDepartamento synchronizingDepartamento = new SynchronizingDepartamento();

        public static async Task<string> SetDepartamentos()
        {
            return await synchronizingDepartamento.SetDepartamentos();
        }
    }
}