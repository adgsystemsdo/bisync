﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BISync.Models;
using Newtonsoft.Json.Linq;

namespace BISync.Services
{
    class NomFamiliaresServices
    {
        static SynchronizingNomFamiliares synchronizingNomFamiliar = new SynchronizingNomFamiliares();

        public static async Task<string> SetNomFamiliares()
        {
            return await synchronizingNomFamiliar.SetNomFamiliares();
        }
    }
}
