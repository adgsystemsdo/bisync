﻿using BISync.Models;
using System.Threading.Tasks;

namespace BISync.Services
{
    class TransaccionesBancosServices
    {
        static SynchronizingTransaccionesBancos synchronizingTransaccionesBancos = new SynchronizingTransaccionesBancos();
        public static async Task<string> SetTransaccionesBancos()
        {
            return await synchronizingTransaccionesBancos.SetTransacciones();
        }
    }
}
