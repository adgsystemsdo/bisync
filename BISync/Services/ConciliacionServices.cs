﻿using BISync.Models;
using System.Threading.Tasks;

namespace BISync.Services
{
    class ConciliacionServices
    {
        static SynchronizingConciliacion synchronizingConciliacion = new SynchronizingConciliacion();
        public static async Task<string> SetConciliaciones()
        {
            return await synchronizingConciliacion.SetConciliaciones();
        }
    }
}
