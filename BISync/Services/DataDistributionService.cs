﻿using BISync.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using BISync.Helpers;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace BISync.Services
{
    public class DataDistributionService
    {
        public static DataTable DataSaleDistributionService()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_VENTA_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataCuadreDistributionServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_CUADRE_CAJA_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataCxcFacturasServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_CXC_FACTURAS_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataCxpFacturasServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_CXP_FACTURAS_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataInvCompraServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_INV_COMPRAS_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataInvDevCompraServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_INV_DEV_COMPRAS_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataCxcMovHeadServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_CXC_MOV_HEAD_FECHA_QUERY);
        }

        public static DataTable DataCxcMovItemServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_CXC_MOV_ITEM_SEGUN_FECHA_QUERY);
        }

        public static DataTable DataCxpMovHeadServices()
        {
            return DataDistribution.ExecuteDataDistribution(DataDistribution.FN_DISTRIBUCION_CXP_MOV_HEAD);
        }


        public static async Task<bool> ExecuteDistributionOnCloud()
        {
            JObject jsonResponce;
            string json = "";

            json += "'" + DataDistribution.FN_DISTRICUTION + "': " + JsonConvert.SerializeObject(DataDistribution.FN_DISTRICUTION, Formatting.Indented) + "";
            jsonResponce = await SettingsGetter.DistributionOnServer(@"{ '" + DataDistribution.FN_DISTRICUTION + "' : { " + json + "}}", SettingsGetter.ApiClientUrl()+ DataDistribution.FN_DISTRICUTION, "ExecuteDistributionOnServises");
            SettingsGetter.Notify("Distribuyendo los datos en la nube: " + jsonResponce.SelectToken("status"), false).Wait();

            return jsonResponce.SelectToken("status").ToString() == "success" ? true : false;
        }

    }
}
