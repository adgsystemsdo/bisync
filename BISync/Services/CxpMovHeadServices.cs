﻿using System.Threading.Tasks;
using BISync.Models;

namespace BISync.Services
{
    class CxpMovHeadServices
    {
        static SynchronizingCxpMovHead synchronizingCxpMovHead = new SynchronizingCxpMovHead();

        public static async Task<string> SetCxpMovHeadDiaria()
        {
            return await synchronizingCxpMovHead.SetCxPMovHeadDiarias();
        }

        public static async Task<string> SetCxpMovHeadMensual()
        {
            return await synchronizingCxpMovHead.SetCxpMovHeadMensual();
        }

        public static async Task<string> SetCxpMovHeadAnuales()
        {
            return await synchronizingCxpMovHead.SetCxpMovHeadAnual();
        }

        public static async Task<string> SetCxpMovHeadHistorico()
        {
            return await synchronizingCxpMovHead.SetCxpMovHeadHistorico();
        }

    }
}
