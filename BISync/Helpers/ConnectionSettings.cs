﻿using Newtonsoft.Json.Linq;

namespace BISync.Helpers
{
    public class ConnectionSettings
    {
        public static string ConnectionSettingsString()
        {
            JObject json = SettingsGetter.JsonSettings();

            string connString = "Host=" + (string)json.SelectToken("DataBase.Host") +
                ";Port=" + (string)json.SelectToken("DataBase.Port") +
                ";Username=" + (string)json.SelectToken("DataBase.Username") +
                ";Password=" + (string)json.SelectToken("DataBase.Password") +
                ";Database=" + (string)json.SelectToken("DataBase.Database") + "";
            return connString;
        }

    }
}
