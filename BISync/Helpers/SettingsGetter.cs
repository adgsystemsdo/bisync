﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using System.IO;
using System;
using System.Text;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Data;
using System.Linq;
using System.Net;

namespace BISync.Helpers
{
    public class SettingsGetter
    {
        public static JObject JsonSettings()
        {
            // read JSON directly from a file
            //Console.WriteLine(Directory.GetCurrentDirectory());
            JObject json;
            using (StreamReader file = File.OpenText("" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON)))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                json = (JObject)JToken.ReadFrom(reader);
            }

            return json;
        }

        public static JObject JsonSettingsTest()
        {
            // read JSON directly from a file
            JObject json;
            using (StreamReader file = File.OpenText("" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON)))
            using (JsonTextReader reader = new JsonTextReader(file))
            {
                json = (JObject)JToken.ReadFrom(reader);
            }

            return json;
        }

        private static JObject CreateJsonSettings()
        {
            string json = @"{
                  'DataBase': {
                    'Host': '10.0.0.252',
                    'Port': '5433',
                    'Username': 'postgres',
                    'Password': 'linux',
                    'Database': 'adg_cruzayala'
                  },
                  'WebHostDirect': {
                    'Host': '10.0.0.250',
                    'Port': '54320',
                    'Username': 'postgres',
                    'Password': 'secret',
                    'Database': 'adgbi_client'
                  },
                  'WebService': {
                    'hosturl': 'http://test.adgbi.test',
                    'port': ':8000',
                    'api': '/api/v1',
                    'portused': 'true',
                    'email': 'soporte@gmail.com',
                    'password': '789456123',
                    'token': 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjEsImlzcyI6Imh0dHA6Ly90ZXN0LmFkZ2JpLnRlc3Q6ODAwMC9hcGkvdjEvbG9naW4iLCJpYXQiOjE1Mzc1NTg2NjksImV4cCI6MTUzNzY0NTA2OSwibmJmIjoxNTM3NTU4NjY5LCJqdGkiOiJ3a3lKN3pRVUN0MmxuenFkIn0.nJ690zjw0AWFh3f9048TmomPLJUDdkN-2hWvG9V1zyA',
                    'firstLogin': 'false'
                  },
                  'DataSync': {
                    'Sales': 'true',
                    'SaleReturn': 'true',
                    'SaleHistory': 'false',
                    'SaleDHistory': 'false',
                    'Cost': 'true',
                    'CostReturn': 'true',
                    'Cuentas_x_cobrar': 'false,
                    'Cuentas_x_pagar': 'false',
                    'Compras': 'false',
                    'Cajas': 'false',
                    'Bancos': 'true',
                    'Nomina': 'false',
                    'Devoluciones': 'false',
                    'Movimientos_cxc': 'false',
                    'Movimientos_cxp': 'false'
                  },
                  'Telegram': {
                    'TelegramBotTest': '839997877:AAEIpSFIJ7xirByn2Xl4zwA7oRnQwZW9I60',
                    'TelegramBotClient': '839997877:AAEIpSFIJ7xirByn2Xl4zwA7oRnQwZW9I60',
                    'chatID': '185638584',
                    'chatID2': '185638584',
                    'chatIDGroup': '-245252257',
                    'Test': 'true',
                    'Group': 'false',
                    'Active': 'true'
                  },
                  'Client': {
                    'Name': ' ',
                    'ShortName': ' ',
                    'Telephone': 'Tel:',
                    'Mail': ' ',
                    'Adreess': ' ',
                    'Adreess2': ' '
                  },
                  'Configurations': {
                    'Time': [{
                    'first': '6'
                        },
                    {
                    'second': '12'
                    },
                    {
                    'third': '18'
                    }],
                    'MainTimer': '300000',
                    'LogTime': '1',
                    'Production': 'true'
                  }
                }";

            json = json.Replace((char)39, (char)34);
            // serialize JSON to a string and then write string to a file
            File.WriteAllText(@"" + Path.Combine(Directory.GetCurrentDirectory(), JsonConst.CLIENT_SECRETS_JSON), json);

            return JsonSettings();
        }

        public static JObject CreateJsonMessage(string status, string message)
        {
            string json = @"{
                              'status': '" + status + @"',
							  'message': '" + message + @"',
                            }";
            // serialize JSON to a string and then write string to a file    
            return JObject.Parse(json);
        }

        public static string ApiLoginUrl()
        {
            JObject json = JsonSettings();

            // serialize JSON to a string and then write string to a file    
            return (string)json.SelectToken("WebService.hosturl") +
                ((bool)json.SelectToken("WebService.portused") ? (string)json.SelectToken("WebService.port") : "") +
                (string)json.SelectToken("WebService.api") + "/login";
        }

        public static string ApiBaseUrl()
        {
            JObject json = JsonSettings();

            // serialize JSON to a string and then write string to a file    
            return (string)json.SelectToken("WebService.hosturl") +
                ((bool)json.SelectToken("WebService.portused") ? (string)json.SelectToken("WebService.port") : "") +
                (string)json.SelectToken("WebService.api") + "/";
        }

        public static string ApiClientUrl()
        {
            JObject json = JsonSettings();

            // serialize JSON to a string and then write string to a file    
            return (string)json.SelectToken("WebService.hosturl") + //json.SelectToken("WebService.clienturl") +
                ((bool)json.SelectToken("WebService.portused") ? (string)json.SelectToken("WebService.port") : "") +
                (string)json.SelectToken("WebService.api") + "/";
        }

        public static string GetApiToken()
        {
            JObject json = JsonSettings();

            // serialize JSON to a string and then write string to a file    
            return (string)json.SelectToken("WebService.token");
        }

        public static void SerializeJsonIntoStream(object value, Stream stream)
        {
            using (var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None })
            {
                var js = new JsonSerializer();
                js.Serialize(jtw, value);
                jtw.Flush();
            }
        }

        public static HttpContent CreateHttpContent(object content)
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                var ms = new MemoryStream();
                SerializeJsonIntoStream(content, ms);
                ms.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(ms);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            }

            return httpContent;
        }

        public static async Task<JObject> DistributionOnServer(string name, string URL, string method)
        {
            JObject jsonResponce;
            Telegram.Bot.Types.Message message;

            try
            {
                HttpClientHandler httpClientHandler = new HttpClientHandler
                {
                    AllowAutoRedirect = false,
                    UseProxy = false
                };

                HttpClient client = new HttpClient(httpClientHandler)
                {
                    Timeout = TimeSpan.FromMinutes(5)
                };

                JObject json = JObject.Parse(name);

                using (var httpContent = new HttpRequestMessage(HttpMethod.Post, URL))
                using (var Content = CreateHttpContent(json))
                {
                    httpContent.Headers.ExpectContinue = false;
                    httpContent.Headers.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    httpContent.Headers.Add("Authorization", $"Bearer {GetApiToken()}");
                    httpContent.Content = Content;
                    httpContent.Version = HttpVersion.Version10;
                    using (HttpResponseMessage response = await client.SendAsync(httpContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseString = await response.Content.ReadAsStringAsync();
                            jsonResponce = JObject.Parse(responseString);
                        }
                        else
                        {
                            var responseString = await response.Content.ReadAsStringAsync();

                            try
                            {
                                jsonResponce = JObject.Parse(responseString);
                            }
                            catch
                            {
                                jsonResponce = CreateJsonMessage("fail", "Method: " + method + ", " + responseString);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                jsonResponce = CreateJsonMessage("fail", "Method: " + method + ", " + ex.Message);

                if ((bool)JsonSettings().SelectToken("Telegram.Active"))
                {
                    message = await TelegramTasks.ApiAsyncPersonalice("Error," + ex.Message, false);
                }
                else
                {
                    Console.WriteLine("==============================================================================");
                    Console.WriteLine(ex.Message);
                }
            }

            return jsonResponce;
        }

        public static async Task<JObject> SendData(DataTable data, string header, string URL, string method)
        {
            JObject jsonResponce;
            Telegram.Bot.Types.Message message;

            try
            {
                HttpClientHandler httpClientHandler = new HttpClientHandler
                {
                    AllowAutoRedirect = false,
                    UseProxy = false
                };

                HttpClient client = new HttpClient(httpClientHandler)
                {
                    Timeout = TimeSpan.FromMinutes(5)
                };

                JObject json = JObject.Parse("{ '" + header + "': " + JsonConvert.SerializeObject(data, Formatting.Indented) + "}");

                using (var httpContent = new HttpRequestMessage(HttpMethod.Post, URL))
                using (var Content = CreateHttpContent(json))
                {
                    httpContent.Headers.ExpectContinue = false;
                    httpContent.Headers.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    httpContent.Headers.Add("Authorization", $"Bearer {GetApiToken()}");
                    httpContent.Content = Content;
                    httpContent.Version = HttpVersion.Version10;
                    using (HttpResponseMessage response = await client.SendAsync(httpContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseString = await response.Content.ReadAsStringAsync();
                            jsonResponce = JObject.Parse(responseString);
                        }
                        else
                        {
                            var responseString = await response.Content.ReadAsStringAsync();

                            try
                            {
                                jsonResponce = JObject.Parse(responseString);
                            }
                            catch
                            {
                                jsonResponce = CreateJsonMessage("fail", "Method: " + method + ", " + responseString);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                jsonResponce = CreateJsonMessage("fail", "Method: " + method + ", " + ex.Message);

                if ((bool)JsonSettings().SelectToken("Telegram.Active"))
                {
                    message = await TelegramTasks.ApiAsyncPersonalice("Error," + ex.Message, false);
                }
                else
                {
                    Console.WriteLine("==============================================================================");
                    Console.WriteLine(ex.Message);
                }
            }

            return jsonResponce;
        }

        public static DataSet SplitDataMethod(DataTable src, int increasement)
        {
            int curVal = 1;

            //temporary object type of datarow
            DataRow[] result = null;

            //destination dataset, here we'll store smaller datatables
            DataSet ds = new DataSet();
            do
            {
                result = src.AsEnumerable()
                    .Where(x => x.Field<int>("id") >= curVal && x.Field<int>("id") < curVal + increasement)
                    .ToArray();
                if (result.Length > 0)
                {
                    DataTable tmp = src.Clone();
                    tmp = result.CopyToDataTable();
                    ds.Tables.Add(tmp);
                }
                curVal += increasement;
            } while (result.Length > 0);

            foreach (DataTable table in ds.Tables)
            {
                table.Columns.Remove("id");
            }

            return ds;
        }

        public static async Task Notify(string sms, bool overwrite)
        {
            if ((bool)JsonSettings().SelectToken("Telegram.Active"))
            {
                Telegram.Bot.Types.Message message;
                message = await TelegramTasks.ApiAsyncPersonalice(sms, overwrite);
            }
            else
            {
                if (overwrite)
                {
                    Console.Write("\r" + sms + "                                                                                                             ");
                }
                else
                {
                    Console.WriteLine(sms);
                }
            }
        }

        public static async Task WriteDoubleLine()
        {
            Telegram.Bot.Types.Message message;
            message = await TelegramTasks.ApiAsyncPersonalice("===============================", false);
        }
    }
}