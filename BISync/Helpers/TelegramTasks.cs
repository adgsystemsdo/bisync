﻿using System;
using System.Threading.Tasks;
using Telegram.Bot;
using Telegram.Bot.Types;

namespace BISync.Helpers
{
    public class TelegramTasks
    {
        static Newtonsoft.Json.Linq.JObject setting = SettingsGetter.JsonSettings();

        private static readonly TelegramBotClient Bot =
            new TelegramBotClient((bool)setting.SelectToken("Telegram.Test") ? (string)setting.SelectToken("Telegram.TelegramBotTest") :
                (string)setting.SelectToken("Telegram.TelegramBotClient"));

        private static readonly string chatIDTestGroup =
            (bool)setting.SelectToken("Telegram.Test") ? (string)setting.SelectToken("Telegram.chatID2") :
                (bool)setting.SelectToken("Telegram.Group") ? (string)setting.SelectToken("Telegram.chatIDGroup") : (string)setting.SelectToken("Telegram.chatID");
        
        private static readonly string chatID = chatIDTestGroup;

        public static async Task<Message> ApiAsync()
        {
            var me = await Bot.GetMeAsync();
            Console.WriteLine($"Hello! My name is {me.FirstName}");
            return await Bot.SendTextMessageAsync(chatID, $"Hello! My name is {me.FirstName}");
        }

        public static async Task<Message> ApiAsyncPersonalice(string Message, bool overwrite)
        {
            var me = await Bot.GetMeAsync();

            if (overwrite)
            {
                Console.Write("\r" + Message + "                                                                                                       ");
            }
            else
            {
                Console.WriteLine(Message.ToString());
            }
            
            return await Bot.SendTextMessageAsync(chatID, Message.ToString());

        }
        
    }
}
