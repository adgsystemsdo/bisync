﻿using Npgsql;
using System.Data;

namespace BISync.Helpers
{
    class ExecuteDataTable
    {
        public static DataTable ExecuteQuery(string query, string tableName)
        {
            DataTable records = new DataTable();

            using ( var conn = new NpgsqlConnection(ConnectionSettings.ConnectionSettingsString()))
            {
                conn.Open();
                // Retrieve all rows
                using (var cmd = new NpgsqlCommand(query, conn))
                {
                    cmd.CommandTimeout = 0;
                    using (var myReader = cmd.ExecuteReader())
                    {
                        records.Load(myReader);
                        records.TableName = tableName;
                        myReader.Close();
                        return records;
                    }
                }
            }
        }

    }
}
