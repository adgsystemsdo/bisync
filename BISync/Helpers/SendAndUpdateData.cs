﻿using BISync.Controllers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Data;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace BISync.Helpers
{
    class SendAndUpdateData
    {
        public string ConfirmationMessage { get; set; }
        public string UpdateQueryData { get; set; }

        public static async Task<JObject> SendDataToServer(DataTable data, string process)
        {
            JObject jsonResponce;
            if (data.Rows.Count > 0)
            {
                string URL = SettingsGetter.ApiClientUrl() + data.TableName ;
                DataSet ds = SettingsGetter.SplitDataMethod(data, 30000);

                decimal count = ((decimal)100 / (decimal)ds.Tables.Count);
                decimal progress = 0;

                jsonResponce = SettingsGetter.CreateJsonMessage(" ", " ");

                SettingsGetter.Notify("0% -> 100%: " + DateTime.Now, false).Wait();

                foreach (DataTable table in ds.Tables)
                {
                    jsonResponce = await SettingsGetter.SendData(table, data.TableName, URL, process);

                    if ((string)jsonResponce.SelectToken("status") == "fail" && (string)jsonResponce.SelectToken("message") == "Token expirada")
                    {
                        bool result = await UserControllers.LoginAsync(SettingsGetter.JsonSettings());

                        if (result)
                        {
                            jsonResponce = await SettingsGetter.SendData(table, data.TableName, URL, process);
                        }
                    }
                    else if ((string)jsonResponce.SelectToken("status") != "success")
                    {
                        return jsonResponce;
                    }
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    progress = (progress + count);
                    SettingsGetter.Notify(progress.ToString("0.##") + "% -> 100%: " + DateTime.Now, true).Wait();
                    Console.SetCursorPosition(0, Console.CursorTop);
                }
            }
            else
            {
                jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method: " + process + ", No hay datos para sincronizar");
            }

            return jsonResponce;

        }

        private static async Task<bool> UpdateData(DataTable myTable, string header, string query)
        {
            try
            {
                bool updated = false;

                JObject json = JObject.Parse(" {'" + header + "': " + JsonConvert.SerializeObject(myTable, Formatting.Indented) + "}");

                using (var conn = new NpgsqlConnection(ConnectionSettings.ConnectionSettingsString()))
                {
                    conn.Open();
                    conn.TypeMapper.UseJsonNet();

                    DataTable tableLoad = new DataTable();
                    // Retrieve all rows
                    using (var cmd = new NpgsqlCommand(query, conn))
                    {
                        cmd.CommandTimeout = 0;
                        cmd.Parameters.AddWithValue("json", NpgsqlDbType.Json, json);
                        cmd.Parameters.AddWithValue("header", NpgsqlDbType.Text, header);
                        using (var myReader = cmd.ExecuteReader())
                        {
                            tableLoad.Load(myReader);
                            myReader.Close();

                            updated = await Task.FromResult(tableLoad.Rows[0].Field<bool>(0));
                        }
                    }
                }

                return updated;
            }
            catch (Exception ex)
            {
                JObject jsonResponce = SettingsGetter.CreateJsonMessage("SQLError", ex.Message);
                SettingsGetter.Notify(JsonConvert.SerializeObject(jsonResponce, Formatting.Indented), false).Wait();
                return false;
            }
        }

        public async Task<string> Update(DataTable myTable, string proceso)
        {
            JObject jsonResponse = await SendAndUpdateData.SendDataToServer(myTable, proceso);
            bool updated = false;

            if ((string)jsonResponse.SelectToken("status") == "success")
                updated = await UpdateData(myTable, myTable.TableName, UpdateQueryData);

            return (string)jsonResponse.SelectToken("message");
        }

        public static async Task<JObject> SendAuxiliaresData(string data, string URL, string method)
        {
            JObject jsonResponce;
            Telegram.Bot.Types.Message message;

            try
            {
                HttpClientHandler httpClientHandler = new HttpClientHandler
                {
                    AllowAutoRedirect = false,
                    UseProxy = false
                };

                HttpClient client = new HttpClient(httpClientHandler)
                {
                    Timeout = TimeSpan.FromMinutes(10)
                };

                JObject json = JObject.Parse(data);

                using (var httpContent = new HttpRequestMessage(HttpMethod.Post, URL))
                using (var Content = SettingsGetter.CreateHttpContent(json))
                {
                    httpContent.Headers.ExpectContinue = false;
                    httpContent.Headers.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                    httpContent.Headers.Add("Authorization", $"Bearer {SettingsGetter.GetApiToken()}");
                    httpContent.Content = Content;
                    httpContent.Version = HttpVersion.Version10;
                    using (HttpResponseMessage response = await client.SendAsync(httpContent))
                    {
                        if (response.IsSuccessStatusCode)
                        {
                            var responseString = await response.Content.ReadAsStringAsync();
                            jsonResponce = JObject.Parse(responseString);
                        }
                        else
                        {
                            var responseString = await response.Content.ReadAsStringAsync();
                            try
                            {
                                jsonResponce = JObject.Parse(responseString);
                            }
                            catch
                            {
                                jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method: " + method + ", " + responseString);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                jsonResponce = SettingsGetter.CreateJsonMessage("fail", "Method: " + method + ", " + ex.Message);

                if ((bool)SettingsGetter.JsonSettings().SelectToken("Telegram.Active"))
                {
                    message = await TelegramTasks.ApiAsyncPersonalice("Error," + ex.Message, false);
                }
                else
                {
                    Console.WriteLine("==============================================================================");
                    Console.WriteLine(ex.Message);
                }
            }

            return new JObject(); //--jsonResponce;
        }

    }

}
