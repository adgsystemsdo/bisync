﻿using BISync.Models;
using Refit;
using System.Threading.Tasks;

namespace BISync.Interfaces
{
    [Headers("User-Agent: Auxiliar Interfaces")]
    interface IAuxiliarInterface
    {
        //--http://10.0.0.249:8300/

        [Post("/api/v1/auxiliares")]
        [Headers("Authorization: Bearer")]
        Task<AuxiliaresResponse> Auxiliares([Body] string data);

    }
}
